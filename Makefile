IMAGE_NAME := virtual_buddy

docker_build:
	docker build -t $(IMAGE_NAME) .

docker_explore:
	docker run -it --rm --entrypoint=/bin/bash $(IMAGE_NAME)

check:

	black src
	black tests

	mypy src
	mypy tests

	ruff check src
	ruff check tests

	pytest tests -x -vv

backend:
	python src/virtual_buddy/microservices.py

frontend:
	streamlit run ui/webapp.py
