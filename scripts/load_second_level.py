from virtual_buddy.pipelines.loading_pipeline.document_loading import (
    create_second_level_map,
)
from virtual_buddy.retrieval.embedder.core import Embedder
from virtual_buddy.document.knowledge_base import KnowledgeBase, store_kb, load_kb
from virtual_buddy.retrieval.vectorstore.weaviate_vectorstore import (
    WeaviateVectorstore,
)
from virtual_buddy.retrieval.embedder import get_embedder
import numpy as np


kb = load_kb("./data/kb/KB_Handbook.pkl")
vectorstore = WeaviateVectorstore(500)
embedder = get_embedder(
    {
        "embedder_class": "SBertEmbedder",
        "model_name": "all-MiniLM-L6-v2",
    }
)

# vectorstore.reset_vectorstore("ChapterEmbedding")
create_second_level_map(kb, vectorstore, embedder)
