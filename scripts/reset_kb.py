from virtual_buddy.pipelines.loading_pipeline.document_loading import (
    load_all_documents,
    reset_kb,
    reset_indexed_memory,
)
from virtual_buddy.data_ingestion.atomizer import get_atomizer
from virtual_buddy.settings import ConfigManager
from virtual_buddy.document.knowledge_base import KnowledgeBase
from virtual_buddy.agents.llm_imagetext import LLM_Image2Text
from virtual_buddy.retrieval.vectorstore.weaviate_vectorstore import WeaviateVectorstore
from virtual_buddy.retrieval.embedder import get_embedder
from virtual_buddy.retrieval.embedder.core import Embedder
from virtual_buddy.retrieval.retriever.indexed_memory import (
    create_indexed_memory,
)

conf_manager = ConfigManager()

secret_api_key = conf_manager.get_secret_config("api_key")
api_key = secret_api_key.api_key.get_secret_value()


def init_vectorstore(
    kb: KnowledgeBase, vector_store: WeaviateVectorstore, embedder: Embedder
) -> None:
    indexed_memory = create_indexed_memory(kb, vector_store, embedder)
    vector_store.reset_vectorstore(class_name="EmbeddingWithMetadata")
    vector_store.populate_vectorstore(
        indexed_memory.all_embedding_with_metadata,
        class_name="EmbeddingWithMetadata",
    )


img2text = LLM_Image2Text(
    model_name=conf_manager.get_config("llm_image2text")["model_name"],
    api_key=api_key,
    personas=conf_manager.get_config("llm_image2text")["personas"],
    temperature=conf_manager.get_config("llm_image2text")["temperature"],
)

all_documents = load_all_documents(
    conf_manager.get_config("downloader_step")["source_path"],
    conf_manager.get_config("downloader_step")["destination_path"],
    conf_manager.get_config("parser_step")["granularity_level"],
    get_atomizer(conf_manager.get_config("atomizer_step")["atomizer"]),
    conf_manager.get_config("atomizer_step")["atom_size"],
    conf_manager.get_config("atomizer_step")["overlap_size"],
    img2text=img2text,
)

kb = KnowledgeBase(
    name=conf_manager.get_config("dump_step")["kb_name"], documents=all_documents
)

reset_kb(
    all_documents,
    conf_manager.get_config("dump_step")["kb_name"],
    conf_manager.get_config("dump_step")["kb_store_path"],
)

vectorstore = WeaviateVectorstore(500)
embedder = get_embedder(conf_manager.get_config("embedder"))

init_vectorstore(kb, vectorstore, embedder)
