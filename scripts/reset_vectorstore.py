from virtual_buddy.settings import ConfigManager
from virtual_buddy.document.knowledge_base import load_kb
from virtual_buddy.retrieval.embedder import get_embedder
from virtual_buddy.pipelines.loading_pipeline.document_loading import (
    reset_vectorstore,
)
from virtual_buddy.retrieval.vectorstore.weaviate_vectorstore import (
    WeaviateVectorstore,
)

conf_manager = ConfigManager()

dump_step_conf = conf_manager.get_config("dump_step")
kb_name = dump_step_conf["kb_name"]
kb_store_path = dump_step_conf["kb_store_path"]

kb = load_kb(f"{kb_store_path}/{kb_name}.pkl")
embedder = get_embedder(conf_manager.get_config("embedder"))
vectorstore = WeaviateVectorstore(load_batch_size=500)

reset_vectorstore(kb, vectorstore, embedder)
