from virtual_buddy.document.knowledge_base import KnowledgeBase, store_kb, load_kb
from virtual_buddy.agents.llm_summarizer import LLM_Summarizer
from virtual_buddy.settings import ConfigManager
import time


config_manager = ConfigManager()

# Secret api key
chatbot_conf = config_manager.get_config("llm_summarizer")
secret_api_key = config_manager.get_secret_config("api_key")
api_key = secret_api_key.api_key.get_secret_value()
model_name = chatbot_conf["model_name"]
personas = chatbot_conf["personas"]
temperature = chatbot_conf["temperature"]


kb = load_kb("./data/kb/KB_Handbook.pkl")
llm = LLM_Summarizer(model_name, api_key, personas, temperature)

for doc in kb.documents:
    for chapter in doc.chapters:
        response_content, response_reason_finish, total_token_usage = llm.send_message(
            chapter.text
        )
        chapter.summarized_text = response_content
        print(response_content)
        store_kb(kb, "./data/kb", overwrite=True)
        time.sleep(50)
