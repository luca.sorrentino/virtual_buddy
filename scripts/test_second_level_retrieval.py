from virtual_buddy.retrieval.retriever.strategies.emb_retrieve_strategy import (
    SecondLevelRetrieveStrategy,
    NaiveRetrieveStrategy,
)
from virtual_buddy.retrieval.embedder.core import Embedder
from virtual_buddy.retrieval.embedder.core import Embedder
from virtual_buddy.document.knowledge_base import KnowledgeBase, store_kb, load_kb
from virtual_buddy.retrieval.vectorstore.weaviate_vectorstore import (
    WeaviateVectorstore,
)
from virtual_buddy.retrieval.embedder import get_embedder
import numpy as np


top_n = 10
kb = load_kb("./data/kb/KB_Handbook.pkl")
vectorstore = WeaviateVectorstore(500)
embedder = get_embedder(
    {
        "embedder_class": "SBertEmbedder",
        "model_name": "all-MiniLM-L6-v2",
    }
)

query = "what are the benefits in Agilelab?"
embedded_query = embedder.embed(query)
# retrieva_strategy = SecondLevelRetrieveStrategy(vectorstore)
retrieva_strategy = SecondLevelRetrieveStrategy(vectorstore)
retrieved_docs = retrieva_strategy(embedded_query, top_n)

for doc in retrieved_docs:
    print(doc["document_name"])
