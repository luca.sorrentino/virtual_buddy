from virtual_buddy.pipelines.loading_pipeline.document_loading import load_all_documents
from virtual_buddy.data_ingestion.atomizer import get_atomizer
from virtual_buddy.settings import ConfigManager
from virtual_buddy.document.knowledge_base import KnowledgeBase, store_kb

conf_manager = ConfigManager()


def create_new_kb(conf_manager, initializer, img2text):
    all_documents = load_all_documents(
        conf_manager.get_config("downloader_step")["source_path"],
        conf_manager.get_config("downloader_step")["destination_path"],
        conf_manager.get_config("parser_step")["granularity_level"],
        get_atomizer(conf_manager.get_config("atomizer_step")["atomizer"]),
        conf_manager.get_config("atomizer_step")["atom_size"],
        conf_manager.get_config("atomizer_step")["overlap_size"],
        img2text=img2text,
    )

    # Create new knowledge Base
    kb = KnowledgeBase(
        name=conf_manager.get_config("dump_step")["kb_name"], documents=all_documents
    )
    store_kb(kb, conf_manager.get_config("dump_step")["kb_store_path"])
    initializer.kb = kb
