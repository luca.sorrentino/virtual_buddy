from virtual_buddy.chatbot import Chatbot
from virtual_buddy.retrieval.retriever.strategies.emb_retrieve_strategy import (
    NaiveRetrieveStrategy,
    HibrydRetrieveStrategy,
    RerankingWithTitles,
    RerankingWithDocName,
    RerankingCrossEncoder,
    NaiveChapterRetrieveStrategy,
    NaiveRetrieveTriplo,
)
from virtual_buddy.document.knowledge_base import KnowledgeBase, store_kb
from virtual_buddy.settings import ConfigManager
from virtual_buddy.retrieval.vectorstore.weaviate_vectorstore import (
    WeaviateVectorstore,
)
import pandas as pd
import numpy as np
from virtual_buddy.retrieval.embedder import get_embedder
from virtual_buddy.pipelines.loading_pipeline.document_loading import (
    load_all_documents,
    reset_first_level_vectorstore,
    create_second_level_map,
)
from virtual_buddy.data_ingestion.atomizer import get_atomizer
import pandas as pd
from virtual_buddy.retrieval.retriever.indexed_memory import (
    create_indexed_memory,
    store_indexed_memory,
    load_indexed_memory,
)
from virtual_buddy.agents.llm_imagetext import LLM_Image2Text


def isin_strategy(retrieved_data, n, correct_answer):
    for data in retrieved_data[:n]:
        if data["document_name"] == correct_answer:
            return True

    return False


def most_frequent_strategy(retrieved_data, n, correct_answer):
    retrieved_document_names = np.array(
        [data["document_name"] for data in retrieved_data[:n]]
    )
    vals, counts = np.unique(retrieved_document_names, return_counts=True)
    index = np.argmax(counts)
    predicted_answer = vals[index]
    correct_match = predicted_answer == correct_answer
    return int(correct_match)


def get_correct_positon(retrieved_data, topn, correct_answer):
    correct_id = -1
    for id, data in enumerate(retrieved_data[:topn]):
        if data["document_name"] == correct_answer:
            correct_id = id + 1  # perchè l' id parte da 0
            break
    return correct_id


def create_new_kb(conf_manager, initializer, img2text):
    all_documents = load_all_documents(
        conf_manager.get_config("downloader_step")["source_path"],
        conf_manager.get_config("downloader_step")["destination_path"],
        conf_manager.get_config("parser_step")["granularity_level"],
        get_atomizer(conf_manager.get_config("atomizer_step")["atomizer"]),
        conf_manager.get_config("atomizer_step")["atom_size"],
        conf_manager.get_config("atomizer_step")["overlap_size"],
        img2text=img2text,
    )

    # Create new knowledge Base
    kb = KnowledgeBase(
        name=conf_manager.get_config("dump_step")["kb_name"], documents=all_documents
    )
    store_kb(kb, conf_manager.get_config("dump_step")["kb_store_path"])
    initializer.kb = kb


def evaluation_loop(
    embedder, initializer, all_questions, topn, true_answers, retrieve_strategy
):
    rank_score = 0
    mrr_score = 0
    counter_matches_most_freq_topn = 0
    counter_top1 = 0
    for id, question in enumerate(all_questions):
        embedded_query = embedder.embed(question).tolist()[0]
        if retrieve_strategy == "NaiveRetrieveStrategy":
            emb_retrieve_strategy = NaiveRetrieveStrategy(initializer.vector_store)
            all_retrieved_data = emb_retrieve_strategy(embedded_query, top_n=topn)
        elif retrieve_strategy == "NaiveRetrieveTriplo":
            emb_retrieve_strategy = NaiveRetrieveTriplo(initializer.vector_store)
            all_retrieved_data = emb_retrieve_strategy(embedded_query, top_n=topn)

        elif retrieve_strategy == "NaiveChapterRetrieveStrategy":
            emb_retrieve_strategy = NaiveChapterRetrieveStrategy(
                initializer.vector_store
            )
            all_retrieved_data = emb_retrieve_strategy(embedded_query, top_n=topn)
        elif retrieve_strategy == "RerankingCrossEncoder":
            print("################################ \n\n ")
            print(f"question: {question}: ({true_answers[id]})")
            # HibrydRetrieveStrategy(vectorstore=initializer.vectorstore, alpha=0.90)

            first_strategy = NaiveRetrieveStrategy(initializer.vector_store)
            first_level_retrieved_data = first_strategy(embedded_query, top_n=topn)
            print("Lista dei documenti al primo stage di ricerca")
            print([data["document_name"] for data in first_level_retrieved_data])

            reranking_strategy = RerankingCrossEncoder(initializer.kb)
            all_retrieved_data = reranking_strategy(
                question, first_level_retrieved_data
            )
            print("Lista dei documenti rerankati")
            print([data["document_name"] for data in all_retrieved_data])
            print("################################ \n\n ")

        elif retrieve_strategy == "RerankingWithDocName":
            # print("################################ \n\n ")
            # print(f"question: {question}: ({true_answers[id]})")

            first_strategy = NaiveRetrieveStrategy(initializer.vector_store)
            first_level_retrieved_data = first_strategy(embedded_query, top_n=topn)
            # print("Lista dei documenti al primo stage di ricerca")
            # print([data["document_name"] for data in first_level_retrieved_data])

            reranking_strategy = RerankingWithDocName(embedder)
            all_retrieved_data = reranking_strategy(
                embedded_query, first_level_retrieved_data
            )
            # print("Lista dei documenti rerankati")
            # print([data["document_name"] for data in all_retrieved_data])
            # print("################################ \n\n ")

        elif retrieve_strategy == "RerankingWithTitles":
            mapper = create_second_level_map(initializer.kb, embedder)
            reranking_strategy = RerankingWithTitles(mapper)
            # print("################################ \n\n ")
            # print(f"question: {question}: ({true_answers[id]})")

            first_strategy = NaiveRetrieveStrategy(initializer.vector_store)
            first_level_retrieved_data = first_strategy(embedded_query, top_n=topn)
            # print("Lista dei documenti al primo stage di ricerca")
            # print([data["document_name"] for data in first_level_retrieved_data])

            # """
            # """
            all_retrieved_data = reranking_strategy(
                embedded_query, first_level_retrieved_data
            )

            # print("Lista dei documenti rerankati")
            # print([data["document_name"] for data in all_retrieved_data])
            # print("################################ \n\n ")
        elif retrieve_strategy == "RerankingWithChapterEmb":
            mapper = create_second_level_map(
                initializer.kb, initializer.vector_store, embedder
            )
            reranking_strategy = RerankingWithTitles(mapper)
            # print("################################ \n\n ")
            # print(f"question: {question}: ({true_answers[id]})")

            first_strategy = NaiveRetrieveStrategy(initializer.vector_store)
            first_level_retrieved_data = first_strategy(embedded_query, top_n=topn)
            # print("Lista dei documenti al primo stage di ricerca")
            # print([data["document_name"] for data in first_level_retrieved_data])

            all_retrieved_data = reranking_strategy(
                embedded_query, first_level_retrieved_data
            )

            # print("Lista dei documenti rerankati")
            # print([data["document_name"] for data in all_retrieved_data])
            # print("################################ \n\n ")
        else:
            emb_retrieve_strategy = HibrydRetrieveStrategy(
                initializer.vector_store, alpha=0.90
            )
            all_retrieved_data = emb_retrieve_strategy(question, embedded_query)

        correct_answer = true_answers[id]
        correct_position = get_correct_positon(all_retrieved_data, topn, correct_answer)
        counter_top1 += int(correct_position < 2)
        print("correct_position: ", correct_position)
        counter_matches_most_freq_topn += most_frequent_strategy(
            all_retrieved_data, topn, correct_answer
        )
        # print(f"{id}) correct_position: {correct_position}")
        """
        if correct_position > 1:
            print(
                f"\nQuestion: {question} \n Answer"
                f" text:{all_retrieved_data[0]['data_atom_text']} \n"
            )
        """
        # print(f"rank_score: {rank_score}")
        if correct_position != -1:
            rank_score += 1 / correct_position
        mrr_score = rank_score / len(all_questions)
    return mrr_score, counter_matches_most_freq_topn, counter_top1


# "all-MiniLM-L6-v2"
def init_embedder(emb_name):

    # embeddings: all-mpnet-base-v2 , all-MiniLM-L6-v2, multi-qa-mpnet-base-dot-v1, multi-qa-MiniLM-L6-dot-v1, msmarco-distilbert-base-tas-b
    embedder_conf = {
        "embedder_class": "SBertEmbedder",
        "model_name": emb_name,
    }
    embedder = get_embedder(embedder_conf)
    return embedder


def initialization():
    conf_manager = ConfigManager()
    initializer = Chatbot()
    df = pd.read_csv("data/dataset/dataset.csv")
    true_answers = df["title"].tolist()
    all_questions = df["question"].tolist()
    return conf_manager, initializer, true_answers, all_questions


## PARAMETRI DA TUNARE
# for emb_retrieve_strategy in ["NaiveRetrieveStrategy","HibrydRetrieveStrategy"]


retrieve_strategy = "HibrydRetrieveStrategy"
all_topn = [10]
all_atom_size = [50]
all_overlap_size = [10]
embedder_name = [
    "thenlper/gte-base",
    "all-MiniLM-L6-v2",
]
# "./data/models/fine_tuned_all_minilm_l6_v2",
do_kb_reset = len(all_atom_size) > 1 or len(all_overlap_size) > 1
do_vectorstore_reset = len(embedder_name) > 1 or do_kb_reset

do_vectorstore_reset = True
do_kb_reset = True


conf_manager, initializer, true_answers, all_questions = initialization()

secret_api_key = conf_manager.get_secret_config("api_key")
api_key = secret_api_key.api_key.get_secret_value()

img2text = LLM_Image2Text(
    model_name=conf_manager.get_config("llm_image2text")["model_name"],
    api_key=api_key,
    personas=conf_manager.get_config("llm_image2text")["personas"],
    temperature=conf_manager.get_config("llm_image2text")["temperature"],
)

mrr_score_stats = []
topn_stats = []
atom_size_stats = []
embedder_name_stats = []
overlap_size_stats = []
kb_reset_flag = []
vectorstore_reset_flag = []


## PIPELINE
for emb_name in embedder_name:
    for atom_size in all_atom_size:
        for overlap_size in all_overlap_size:
            embedder = init_embedder(emb_name)
            initializer.embedder = embedder
            if do_kb_reset:
                conf_manager.get_config("atomizer_step")["atom_size"] = atom_size
                conf_manager.get_config("atomizer_step")["overlap_size"] = overlap_size
                create_new_kb(conf_manager, initializer, img2text)
            if do_vectorstore_reset:
                reset_first_level_vectorstore(
                    initializer.kb, initializer.vector_store, embedder
                )
            for topn in all_topn:
                mrr_score, counter_matches_most_freq_topn, counter_top1 = (
                    evaluation_loop(
                        embedder,
                        initializer,
                        all_questions,
                        topn,
                        true_answers,
                        retrieve_strategy,
                    )
                )
                mrr_score_stats.append(mrr_score)
                topn_stats.append(topn)
                atom_size_stats.append(atom_size)
                overlap_size_stats.append(overlap_size)
                embedder_name_stats.append(emb_name)
                vectorstore_reset_flag.append(do_vectorstore_reset)
                kb_reset_flag.append(do_kb_reset)
                print(
                    f" \n mrr_score: {mrr_score} \n  topn: {topn} \n  atom_size:"
                    f" {atom_size} \n overlap_size: {overlap_size} \n emb_name:"
                    f" {emb_name} \n do_vectorstore_reset: {do_vectorstore_reset} \n"
                    f" do_kb_reset: {do_kb_reset} \n"
                    f" count_matches_most_freq topn {counter_matches_most_freq_topn}\n"
                    f" counter top1: {counter_top1} \n"
                )


df_stats = pd.DataFrame(
    {
        "mrr_score": mrr_score_stats,
        "topn": topn_stats,
        "atom_size": atom_size_stats,
        "overlap_size": overlap_size_stats,
        "embedder_name": embedder_name_stats,
        "kb_reset": kb_reset_flag,
        "vectorstore_reset": vectorstore_reset_flag,
    }
)

df_stats.to_csv("data/dataset/evaluation_0.csv")
print(df_stats)
