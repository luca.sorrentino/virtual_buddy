from virtual_buddy.data_ingestion.atomizer import get_atomizer
from virtual_buddy.pipelines.statistic_pipeline.calculate_statistics import (
    calculate_statistics,
)
from virtual_buddy.pipelines.statistic_pipeline.steps import (
    WordCounter,
    DataAtomCounter,
    ChapterCounter,
    TokensCounterPerChapter,
    TokensCounterPerDocument,
    TokensCounter,
    DocumentCounter,
)
from virtual_buddy.settings import ConfigManager
from virtual_buddy.agents.tokenizer import TikTokenizer
from virtual_buddy.document.knowledge_base import load_kb


# set loguru to info
import loguru

loguru.logger.remove()
loguru.logger.add("logs/info.log", level="INFO", rotation="1 MB")


conf_manager = ConfigManager()

# downloader parameter
downloader_step_conf = conf_manager.get_config("downloader_step")
source_path = downloader_step_conf["source_path"]
destination_path = downloader_step_conf["destination_path"]

# loader parameter
loader_step_conf = conf_manager.get_config("loader_step")
destination_path = loader_step_conf["destination_path"]

# parse parameters
parser_step_conf = conf_manager.get_config("parser_step")
granularity_level = parser_step_conf["granularity_level"]

# split parameters
atomizer_step_conf = conf_manager.get_config("atomizer_step")
atom_size = atomizer_step_conf["atom_size"]
overlap_size = atomizer_step_conf["overlap_size"]
atomizer = get_atomizer(atomizer_step_conf["atomizer"])
# save KB
dump_step_conf = conf_manager.get_config("dump_step")
kb_name = dump_step_conf["kb_name"]
kb_store_path = dump_step_conf["kb_store_path"]

# LLM
llm_conf = conf_manager.get_config("llm_generator")
model_name = llm_conf["model_name"]
max_allowed_tokens = llm_conf["max_allowed_tokens"]

# Statistics
stats_conf = conf_manager.get_config("statistics")
destination_path = stats_conf["destination_path"]
max_allowed_tokens = stats_conf["max_allowed_tokens"]

# Tokenizer
tokenizer_encoding_name = TikTokenizer.get_tokenizer(model_name)
tokenizer = TikTokenizer(encoding_name=tokenizer_encoding_name.name)


kb = load_kb(f"{kb_store_path}/{kb_name}.pkl")

stats = calculate_statistics(
    kb,
    kb_stats_operations=[
        DocumentCounter(),
        TokensCounterPerChapter(tokenizer, destination_path, max_allowed_tokens),
        TokensCounterPerDocument(tokenizer, destination_path, max_allowed_tokens),
        TokensCounter(tokenizer),
    ],
    doc_stats_operations=[ChapterCounter()],
    chapter_stats_operations=[DataAtomCounter()],
    data_atom_stats_operations=[WordCounter()],
)
stats.show()
