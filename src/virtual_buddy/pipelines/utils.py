import time

# set loguru to info
from loguru import logger


def timing_decorator(func):
    def wrapper(*args, **kwargs):
        start_time = time.time()
        result = func(*args, **kwargs)
        end_time = time.time()
        execution_time = end_time - start_time
        print(f"{func.__name__} ha impiegato {execution_time:.6f} secondi")
        return result

    return wrapper
