# from virtual_buddy.pipelines.loading_pipeline.pipeline import IngestionPipeline
from typing import List
from virtual_buddy.document.document import Document
from virtual_buddy.document.chapter import Chapter, ChapterWithDataAtom
from virtual_buddy.data_ingestion.atomizer.core import Atomizer
from virtual_buddy.pipelines.loading_pipeline.steps.download import download_step
from virtual_buddy.pipelines.loading_pipeline.steps.load import loader_step
from virtual_buddy.pipelines.loading_pipeline.steps.parse import parse_step
from virtual_buddy.pipelines.loading_pipeline.steps.img2text import img2text_step
from virtual_buddy.pipelines.loading_pipeline.steps.split import split_step
from virtual_buddy.agents.llm_imagetext import LLM_Image2Text
from virtual_buddy.document.knowledge_base import KnowledgeBase, store_kb
from virtual_buddy.retrieval.embedder.core import Embedder
from virtual_buddy.retrieval.vectorstore.weaviate_vectorstore import (
    WeaviateVectorstore,
)
from virtual_buddy.retrieval.retriever.indexed_memory import (
    create_indexed_memory,
    store_indexed_memory,
)
import numpy as np
from virtual_buddy.retrieval.retriever.emedding_with_metadata import (
    EmbeddingWithMetadata,
    ChapterEmbedding,
    AllPageEmbedding,
)


def create_second_level_map(
    kb: KnowledgeBase,
    vectorstore: WeaviateVectorstore,
    embedder: Embedder,
):
    # vectorstore.add_new_class("ChapterEmbedding")
    # vectorstore.add_new_class("AllPageEmbedding")
    mapper = dict()

    all_chapter_for_vectorscore = []
    all_pages_for_vectorscore = []

    for doc in kb.documents:
        all_page_embs = []

        for chapter in doc.chapters:
            all_atoms_embs = []
            for atom in chapter.data_atoms:
                all_atoms_embs.append(embedder.embed(atom.text)[0])

            chapter_emb = np.mean(all_atoms_embs, axis=0).tolist()

            all_chapter_for_vectorscore.append(
                ChapterEmbedding(
                    chapter_text=chapter.text,
                    embedding_as_list=chapter_emb,
                    document_name=doc.name,
                )
            )
            all_page_embs.append(chapter_emb)

        all_pages_for_vectorscore.append(
            AllPageEmbedding(
                al_page_text="",
                embedding_as_list=np.mean(all_page_embs, axis=0).tolist(),
                document_name=doc.name,
            )
        )

    vectorstore.populate_vectorstore(
        records=all_chapter_for_vectorscore,
        class_name="ChapterEmbedding",
    )

    vectorstore.populate_vectorstore(
        records=all_pages_for_vectorscore,
        class_name="AllPageEmbedding",
    )
    return mapper


def load_all_documents(
    source_path: str,
    destination_path: str,
    granularity_level: int,
    atomizer: Atomizer,
    atom_size: int,
    overlap_size: int,
    img2text: LLM_Image2Text,
) -> List[Document]:
    all_documents: List[Document] = []

    download_step(source_path, destination_path)
    for file in loader_step(destination_path):
        all_file_chapter: List[Chapter] = []

        for chapter in parse_step(file, granularity_level):
            chapter_wo_images = img2text_step(img2text, chapter)
            all_chapter_atoms = split_step(
                chapter_wo_images, atomizer, atom_size, overlap_size
            )
            chapter = ChapterWithDataAtom.init_from_chapter(
                chapter_wo_images, all_chapter_atoms
            )
            all_file_chapter.append(chapter)

        new_document = Document(
            name=file.filename, path=file.file_path, chapters=all_file_chapter
        )
        all_documents.append(new_document)

    return all_documents


def reset_kb(
    all_documents: List[Document], kb_name: str, store_folder_path: str
) -> None:
    kb = KnowledgeBase(name=kb_name, documents=all_documents)
    store_kb(kb, store_folder_path, overwrite=True)


def reset_indexed_memory(
    kb: KnowledgeBase,
    vector_store: WeaviateVectorstore,
    embedder: Embedder,
    store_indexed_memory_path: str,
) -> None:
    indexed_memory = create_indexed_memory(kb, vector_store, embedder)
    store_indexed_memory(indexed_memory, store_indexed_memory_path)


# TODO: spostare questo nella classe vectorstore
def reset_first_level_vectorstore(
    kb: KnowledgeBase, vector_store: WeaviateVectorstore, embedder: Embedder
) -> None:
    indexed_memory = create_indexed_memory(kb, vector_store, embedder)
    vector_store.reset_vectorstore(class_name="EmbeddingWithMetadata")
    vector_store.populate_vectorstore(
        indexed_memory.all_embedding_with_metadata, "EmbeddingWithMetadata"
    )
