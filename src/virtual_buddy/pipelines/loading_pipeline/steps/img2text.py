from virtual_buddy.document.chapter import Chapter
from virtual_buddy.agents.llm_imagetext import LLM_Image2Text
import re
import time


def get_all_imgurls(text: str):
    # Extraction from html tag

    # Pattern regex per trovare gli URL delle immagini in formato Markdown o HTML
    regex_pattern = r'!\[.*?\]\((.*?)\)|<img .*?src="(.*?)"'

    # Trova tutti i match con il pattern regex nel testo combinato
    matches = re.findall(regex_pattern, text)

    # Estrai gli URL delle immagini dagli URL trovati nei match
    image_urls = [url for match in matches for url in match if url]

    valid_url = []
    for url in image_urls:
        if url.split(".")[-1] in ["png", "jpeg", "gif", "webp"]:
            valid_url.append(url)

    # print("valid_url: ", valid_url)
    return valid_url


def img2text_step(llm_img2text: LLM_Image2Text, chapter: Chapter) -> Chapter:
    basic_url = "https://handbook.agilelab.it/"
    extracted_urls = get_all_imgurls(chapter.text)
    for image_url in extracted_urls:
        text_from_img, _, _ = llm_img2text.extract_text_with_cache(basic_url, image_url)
        chapter.text = chapter.text.replace(image_url, text_from_img)
    chapter.reset_id()
    return chapter
