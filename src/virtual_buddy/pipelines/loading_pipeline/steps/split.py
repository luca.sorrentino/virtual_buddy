from virtual_buddy.document.chapter import Chapter
from virtual_buddy.data_ingestion.atomizer.core import Atomizer
from virtual_buddy.pipelines.loading_pipeline.steps.create_atoms import (
    create_atoms,
    create_atoms_with_meta,
)
from virtual_buddy.document.data_atom import DataAtom


def group_atoms(
    all_atoms: list[str], atom_size: int, overlap_size: int
) -> list[list[str]]:
    if atom_size < 0:
        raise ValueError("atom_size must be greater than or equal to 0")

    if overlap_size < 0:
        raise ValueError("overlap_size must be greater than or equal to 0")

    if overlap_size >= atom_size:
        raise ValueError("overlap_size must be less than atom_size")

    if atom_size - overlap_size < 0:
        raise ValueError("atom_size must be greater than overlap_size")

    if type(atom_size) != int:
        raise TypeError("atom_size must be an integer")

    if type(overlap_size) != int:
        raise TypeError("overlap_size must be an integer")

    all_text_atoms = [all_atoms[0:atom_size]]

    real_atom_size = atom_size - overlap_size
    for i in range(atom_size, len(all_atoms), real_atom_size):
        initial_index = max(0, i - overlap_size)
        finish_index = min(i + real_atom_size, len(all_atoms))
        atoms_in_chapter_text = all_atoms[initial_index:finish_index]
        all_text_atoms.append(atoms_in_chapter_text)
    return all_text_atoms


def split_step(
    chapter: Chapter, atomizer: Atomizer, atom_size: int, overlap_size: int
) -> list[DataAtom]:
    if type(chapter.text) != str:
        raise TypeError("chapter text must be a string")

    if len(chapter.text) == 0:
        raise ValueError("given an empty text to split")

    all_atoms = atomizer.split_into_atoms(chapter.text)
    all_text_atoms = group_atoms(all_atoms, atom_size, overlap_size)

    return create_atoms(all_text_atoms, chapter.id)
    # return create_atoms_with_meta(all_text_atoms, chapter.id, chapter)
