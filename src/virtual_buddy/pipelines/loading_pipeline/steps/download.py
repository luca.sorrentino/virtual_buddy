from virtual_buddy.data_ingestion.grabber.handbook_cloner import (
    HandbookCloner,
)


def download_step(source_path: str, destination_path: str) -> None:
    # Download step
    cloner = HandbookCloner()
    cloner.get_data(source_path, destination_path)
