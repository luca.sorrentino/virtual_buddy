from virtual_buddy.document.raw_file import RawFile
from typing import Iterator
from virtual_buddy.data_ingestion.parser.md_parser import MdParser
from virtual_buddy.document.chapter import Chapter


def parse_step(raw_file: RawFile, granularity_level: int) -> Iterator[Chapter]:
    if raw_file.extension == "md":
        parser = MdParser()
    else:
        raise Exception("Extension not supported")

    all_parser_chapter = parser.get_chapters(raw_file.data, granularity_level)
    for parser_chapter in all_parser_chapter:
        chapter = Chapter(title=parser_chapter.title, text=parser_chapter.text)
        yield chapter
