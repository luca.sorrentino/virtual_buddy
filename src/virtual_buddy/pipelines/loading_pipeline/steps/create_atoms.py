import uuid
from virtual_buddy.document.data_atom import DataAtom
from virtual_buddy.document.chapter import Chapter


def create_atoms_with_meta(
    text_atoms: list[list[str]], chapter_id: uuid.UUID, chapter: Chapter
) -> list[DataAtom]:
    if not text_atoms:
        raise ValueError("The text atoms must be at least one")

    first_node = DataAtom(
        text=f"{chapter.document_name}: {text_atoms[0]}",
        prev=None,
        next=None,
        chunck_id=0,
        parent_chapter_id=chapter_id,
    )

    all_nodes: list[DataAtom] = [first_node]
    for index in range(1, len(text_atoms)):
        previous_node = all_nodes[index - 1]
        current_node = DataAtom(
            text=f"{chapter.document_name}: {text_atoms[index]}",
            prev=previous_node,
            next=None,
            chunck_id=index,
            parent_chapter_id=chapter_id,
        )
        previous_node.next = current_node
        all_nodes.append(current_node)

    return all_nodes


def create_atoms(text_atoms: list[list[str]], chapter_id: str) -> list[DataAtom]:
    if not text_atoms:
        raise ValueError("The text atoms must be at least one")

    first_node = DataAtom(
        text=text_atoms[0],
        prev=None,
        next=None,
        chunck_id=0,
        parent_chapter_id=chapter_id,
    )

    all_nodes: list[DataAtom] = [first_node]
    for index in range(1, len(text_atoms)):
        previous_node = all_nodes[index - 1]
        current_node = DataAtom(
            text=text_atoms[index],
            prev=previous_node,
            next=None,
            chunck_id=index,
            parent_chapter_id=chapter_id,
        )
        previous_node.next = current_node
        all_nodes.append(current_node)

    return all_nodes
