from typing import Generator
from virtual_buddy.data_ingestion.reader.general_reader import GeneralReader
from virtual_buddy.document.raw_file import RawFile


def loader_step(destination_path: str) -> Generator[RawFile, None, None]:
    loader = GeneralReader()
    for file in loader.file_reader(
        folder_path=destination_path, allowed_extension="md"
    ):
        yield file
