from virtual_buddy.document.knowledge_base import KnowledgeBase
from abc import ABC, abstractmethod
from virtual_buddy.document.document import Document
from virtual_buddy.document.data_atom import DataAtom
from virtual_buddy.document.chapter import Chapter
from virtual_buddy.document.chapter import ChapterWithDataAtom
from virtual_buddy.agents.tokenizer import Tokenizer
from virtual_buddy.pipelines.statistic_pipeline.plotter import Plotter
from virtual_buddy.pipelines.statistic_pipeline.statistics import Statistics


class KnowledgeBaseOperationStatistics(ABC):
    @abstractmethod
    def run(self, stats: Statistics, kb: KnowledgeBase, *args, **kwargs) -> Statistics:
        return stats


class DocumentStatisticOperation(ABC):
    @abstractmethod
    def run(self, stats: Statistics, document: Document, *args, **kwargs) -> Statistics:
        return stats


class ChapterStatisticOperation(ABC):
    @abstractmethod
    def run(self, stats: Statistics, chapter: Chapter, *args, **kwargs) -> Statistics:
        return stats


class DataAtomStatisticOperation(ABC):
    @abstractmethod
    def run(
        self, stats: Statistics, data_atom: DataAtom, *args, **kwargs
    ) -> Statistics:
        return stats


class WordCounter(DataAtomStatisticOperation):
    def run(
        self, stats: Statistics, data_atom: DataAtom, *args, **kwargs
    ) -> Statistics:
        words_in_data_atom = len(data_atom.text.split())
        stats.words_count += words_in_data_atom
        return stats


class DataAtomCounter(ChapterStatisticOperation):
    def run(self, stats: Statistics, chapter: Chapter, *args, **kwargs) -> Statistics:
        if isinstance(chapter, ChapterWithDataAtom):
            stats.data_atom_count += len(chapter.data_atoms)
        return stats


class ChapterCounter(DocumentStatisticOperation):
    def run(self, stats: Statistics, document: Document, *args, **kwargs) -> Statistics:
        stats.chapter_count += len(document.chapters)
        return stats


class DocumentCounter(KnowledgeBaseOperationStatistics):
    def run(self, stats: Statistics, kb: KnowledgeBase, *args, **kwargs) -> Statistics:
        stats.document_count += len(kb.documents)
        return stats


class TokensCounterPerChapter(KnowledgeBaseOperationStatistics):
    def __init__(
        self, tokenizer: Tokenizer, destination_path: str, max_allowed_tokens: int
    ) -> None:
        super().__init__()
        self.tokenizer: Tokenizer = tokenizer
        self.plotter = Plotter()
        self.destination_path = destination_path
        self.max_allowed_tokens = max_allowed_tokens

    def run(self, stats: Statistics, kb: KnowledgeBase, *args, **kwargs) -> Statistics:
        for document in kb.documents:
            for chapter in document.chapters:
                num_tokens = len(self.tokenizer.encode(chapter.title + chapter.text))
                stats.tokens_per_chapters.append(num_tokens)

        self.plotter.counter_token_distribution(
            f"{self.destination_path}/chapter_token_distribution.html",
            stats.tokens_per_chapters,
            self.max_allowed_tokens,
            title="Distribution of chapter Lengths in tokens",
            xaxis_title="Chapters",
            yaxis_title="tokens per chapters",
        )

        return stats


class TokensCounterPerDocument(KnowledgeBaseOperationStatistics):
    def __init__(
        self, tokenizer: Tokenizer, destination_path: str, max_allowed_tokens: int
    ) -> None:
        super().__init__()
        self.tokenizer: Tokenizer = tokenizer
        self.plotter = Plotter()
        self.destination_path = destination_path
        self.max_allowed_tokens = max_allowed_tokens

    def run(self, stats: Statistics, kb: KnowledgeBase, *args, **kwargs) -> Statistics:
        for document in kb.documents:
            num_tokens_in_doc = 0
            for chapter in document.chapters:
                num_tokens_in_doc += len(
                    self.tokenizer.encode(chapter.title + chapter.text)
                )
            stats.tokens_per_document.append(num_tokens_in_doc)

        self.plotter.counter_token_distribution(
            f"{self.destination_path}/document_token_distribution.html",
            stats.tokens_per_document,
            self.max_allowed_tokens,
            title="Distribution of Document Lengths in tokens",
            xaxis_title="Documents",
            yaxis_title="tokens per documents",
        )

        return stats


class TokensCounter(KnowledgeBaseOperationStatistics):
    def __init__(self, tokenizer: Tokenizer) -> None:
        super().__init__()
        self.tokenizer: Tokenizer = tokenizer

    def run(self, stats: Statistics, kb: KnowledgeBase, *args, **kwargs) -> Statistics:
        for document in kb.documents:
            for chapter in document.chapters:
                num_tokens = len(self.tokenizer.encode(chapter.title + chapter.text))
                stats.tokens_count += num_tokens

        return stats


class DocumentNames(KnowledgeBaseOperationStatistics):
    def run(self, stats: Statistics, kb: KnowledgeBase, *args, **kwargs) -> Statistics:
        for document in kb.documents:
            print(document.name)
        return stats
