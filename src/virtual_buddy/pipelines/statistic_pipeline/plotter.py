import plotly.express as px  # type: ignore
import pandas as pd  # type: ignore
from typing import List


class Plotter:
    def counter_token_distribution(
        self,
        destination_path: str,
        lengths: List[int],
        threshold: int,
        title: str,
        xaxis_title: str,
        yaxis_title: str,
    ) -> None:
        data = pd.DataFrame({"Lengths": lengths})

        fig = px.violin(data, y="Lengths", box=True, points="all")
        fig.add_hline(
            y=threshold,
            line_dash="dash",
            line_color="red",
            annotation_text=f"Max allowed tokens: {threshold}",
            annotation_position="bottom left",
        )

        fig.update_layout(
            title=title,
            xaxis_title=xaxis_title,
            yaxis_title=yaxis_title,
            showlegend=False,
        )

        # html_path = f"{destination_path}/chapter_token_distribution.html"
        fig.write_html(destination_path)
