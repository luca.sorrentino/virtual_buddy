from virtual_buddy.pipelines.statistic_pipeline.steps import (
    Statistics,
    KnowledgeBaseOperationStatistics,
    DocumentStatisticOperation,
    ChapterStatisticOperation,
    DataAtomStatisticOperation,
)
from virtual_buddy.document.knowledge_base import KnowledgeBase
from virtual_buddy.document.chapter import ChapterWithDataAtom
from typing import List


def calculate_statistics(
    kb: KnowledgeBase,
    kb_stats_operations: List[KnowledgeBaseOperationStatistics],
    doc_stats_operations: List[DocumentStatisticOperation],
    chapter_stats_operations: List[ChapterStatisticOperation],
    data_atom_stats_operations: List[DataAtomStatisticOperation],
) -> Statistics:
    statistics = Statistics()

    for kb_operation in kb_stats_operations:
        statistics = kb_operation.run(statistics, kb)

    for doc in kb.documents:
        for doc_operation in doc_stats_operations:
            statistics = doc_operation.run(statistics, doc)

        for chapter in doc.chapters:
            for chapter_operation in chapter_stats_operations:
                statistics = chapter_operation.run(statistics, chapter)

            if isinstance(chapter, ChapterWithDataAtom):
                for data_atom in chapter.data_atoms:
                    for data_atom_operation in data_atom_stats_operations:
                        statistics = data_atom_operation.run(statistics, data_atom)

    return statistics
