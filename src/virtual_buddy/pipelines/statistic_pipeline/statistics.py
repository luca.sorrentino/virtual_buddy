from typing import List


class Statistics:
    def __init__(self) -> None:
        self.words_count: int = 0
        self.data_atom_count: int = 0
        self.chapter_count: int = 0
        self.document_count: int = 0
        self.tokens_per_chapters: List[int] = []
        self.tokens_per_document: List[int] = []
        self.tokens_count: int = 0

    def show(self) -> None:
        print(f"Words count: {self.words_count}")
        print(f"Data atom count: {self.data_atom_count}")
        print(f"Chapter count: {self.chapter_count}")
        print(f"Document count: {self.document_count}")
        print(f"Tokens count: {self.tokens_count}")
