from abc import ABC, abstractmethod
from typing import List, Generator
from virtual_buddy.document.document import Chapter


class Parser(ABC):
    def __init__(self):
        pass

    @abstractmethod
    def get_chapters(
        self, test: List[str], granularity_level: int
    ) -> Generator[Chapter, None, None]:
        pass
