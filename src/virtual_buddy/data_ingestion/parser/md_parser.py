from virtual_buddy.data_ingestion.parser.core import Parser
from typing import List, Generator
import re
from virtual_buddy.document.document import Chapter


FENCES = ["```", "~~~"]
MAX_HEADING_LEVEL = 6


class Line:
    """
    Detects code blocks and ATX headings.

    Headings are detected according to commonmark, e.g.:
    - only 6 valid levels
    - up to three spaces before the first # is ok
    - empty heading is valid
    - closing hashes are stripped
    - whitespace around title is stripped
    """

    def __init__(self, line: str) -> None:
        self.full_line = line
        self._detect_heading(line)

    def _detect_heading(self, line: str) -> None:
        self.heading_level = 0
        self.heading_title = ""
        result = re.search("^[ ]{0,3}(#+)(.*)", line)
        if result is not None and (len(result[1]) <= MAX_HEADING_LEVEL):
            title = result[2]
            if len(title) > 0 and not (title.startswith(" ") or title.startswith("\t")):
                # if there is a title, it must start with a space or tab
                return
            self.heading_level = len(result[1])

            # strip whitespace and closing hashes
            title = title.strip().rstrip("#").rstrip()
            self.heading_title = title

    def is_fence(self) -> bool:
        for fence in FENCES:
            if self.full_line.startswith(fence):
                return True
        return False

    def is_heading(self) -> bool:
        return self.heading_level > 0

    def __repr__(self) -> str:
        return self.full_line


class MdParser(Parser):
    """
    This class is created as a fork of https://github.com/markusstraub/mdsplit/tree/main
    Splits the Markdown text into chapters based on headings.
    The splitter detects headings up to a specified granularity level.

    Args:
        granularity_level (int): The granularity level for splitting.
            Valid values are 1 to 6, where 1 is the highest level (chapter)
            and 6 is the lowest level (subsubsection).

    Returns:
        List[Chapter]: A list of Chapter objects representing the split text.
    """

    def get_chapters(
        self, text: List[str], granularity_level: int
    ) -> Generator[Chapter, None, None]:
        """
        Generates a list of chapters from the Markdown text.

        Each chapter's text includes the heading line.

        Args:
            text (List[str]): The Markdown text to be split.
            granularity_level (int): The granularity level for splitting.
                Valid values are 1 to 6, where 1 is the highest level (chapter)
                and 6 is the lowest level (subsubsection).

        Returns:
            List[Chapter]: A list of Chapter objects representing the split text.
        """
        if granularity_level < 1 or granularity_level > MAX_HEADING_LEVEL:
            raise ValueError(
                f"granularity_level must be between 1 and {MAX_HEADING_LEVEL}"
            )

        max_level = max(1, min(granularity_level, MAX_HEADING_LEVEL))

        curr_parent_headings: List[str] = [""] * MAX_HEADING_LEVEL
        curr_heading_line = None
        curr_lines: List[str] = []
        within_fence = False
        for text_next_line in text:
            next_line = Line(text_next_line)

            if next_line.is_fence():
                within_fence = not within_fence

            is_chapter_finished = (
                not within_fence
                and next_line.is_heading()
                and next_line.heading_level <= max_level
            )
            if is_chapter_finished:
                if len(curr_lines) > 0:
                    all_text = "\n".join(curr_lines[1:])
                    if len(all_text) > 0:
                        yield Chapter(str(curr_heading_line), all_text)

                    # check if curr_parent_headings is empty
                    if curr_heading_line:
                        curr_level = curr_heading_line.heading_level
                        curr_parent_headings[
                            curr_level - 1
                        ] = curr_heading_line.heading_title

                        for level in range(curr_level, MAX_HEADING_LEVEL):
                            curr_parent_headings[level] = ""

                curr_heading_line = next_line
                curr_lines = []

            curr_lines.append(next_line.full_line)
        all_text = "\n".join(curr_lines)
        if len(all_text) > 0:
            yield Chapter(str(curr_heading_line), all_text)
