from pathlib import Path
from typing import Generator
from virtual_buddy.document.raw_file import RawFile
import loguru


class GeneralReader:
    def file_reader(
        self, folder_path: str, allowed_extension: str
    ) -> Generator[RawFile, None, None]:
        folder_path_lib = Path(folder_path)
        for file_path in folder_path_lib.rglob(f"**/*.{allowed_extension}"):
            with file_path.open(mode="r", encoding="utf-8") as file:
                loguru.logger.debug(f"Loading file {file_path}")
                raw_file = RawFile(
                    data=file.readlines(),
                    extension=allowed_extension,
                    path=str(file_path),
                )
                yield raw_file
