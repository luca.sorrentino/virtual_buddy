from typing import Dict
from pathlib import Path
from git import Repo  # type: ignore
import shutil
from virtual_buddy.data_ingestion.grabber.core import Grabber
import markdown_it


class HandbookCloner(Grabber):
    def __init__(self) -> None:
        super().__init__()
        self.temporary_dir: str = "./temp/"
        self.md = markdown_it.MarkdownIt()
        if Path(self.temporary_dir).exists():
            shutil.rmtree(self.temporary_dir, ignore_errors=True)
        Path("./temp/").mkdir(parents=True, exist_ok=True)

    def _parse_level(
        self, parent_dir_path: Path, folder_name: str, filename: str
    ) -> str:
        new_folder_path = parent_dir_path / folder_name
        new_folder_path.mkdir(parents=True, exist_ok=True)
        (Path(self.temporary_dir) / filename).rename(new_folder_path / filename)
        return str(new_folder_path)

    def get_data(
        self, source_path: str, destination_path: str, *args, **kwargs
    ) -> None:
        # clone the repo inside the repo_dir
        Repo.clone_from(source_path, self.temporary_dir)
        summary_file_path = Path(self.temporary_dir) / "SUMMARY.md"

        # Read your Markdown file
        with open(str(summary_file_path), "r", encoding="utf-8") as f:
            markdown_text = f.read()

        tokens = self.md.parse(markdown_text)
        parent_dir_path_map: Dict[int, str] = {3: destination_path}
        new_parent_folder = destination_path

        for token in tokens:
            if token.type == "inline":
                if token and token.children:
                    if len(token.children) >= 2:
                        name_file = str(token.children[0].attrs.get("href"))
                        title = token.children[1].content
                        level = token.level

                        if level in parent_dir_path_map:
                            parent_dir = parent_dir_path_map[level]
                            new_parent_folder = self._parse_level(
                                Path(parent_dir), title, name_file
                            )
                            parent_dir_path_map[level + 2] = new_parent_folder

        # remove the repo folder
        shutil.rmtree(self.temporary_dir, ignore_errors=True)
