from abc import ABC, abstractmethod


class Grabber(ABC):
    @abstractmethod
    def get_data(self, source_path: str, destination_path: str, *args, **kwargs):
        pass
