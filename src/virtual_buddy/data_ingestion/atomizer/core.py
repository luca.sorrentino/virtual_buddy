from abc import ABC, abstractmethod
from typing import Dict, Any


class Atomizer(ABC):
    available_atomizers: Dict[str, Any] = {}

    @abstractmethod
    def split_into_atoms(self, text: str) -> list[str]:
        pass
