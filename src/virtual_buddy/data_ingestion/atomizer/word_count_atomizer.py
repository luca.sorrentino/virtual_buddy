from virtual_buddy.data_ingestion.atomizer.core import Atomizer


class WordCountAtomizer(Atomizer):
    def split_into_atoms(self, text: str) -> list[str]:
        return text.split(" ")
