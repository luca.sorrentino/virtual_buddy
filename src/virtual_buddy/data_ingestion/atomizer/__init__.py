from virtual_buddy.data_ingestion.atomizer.word_count_atomizer import WordCountAtomizer
from virtual_buddy.data_ingestion.atomizer.core import Atomizer


def get_atomizer(atomizer_name: str) -> Atomizer:
    available_atomizers = {
        "WORDCOUNTATOMIZER": WordCountAtomizer,
    }

    # print(f"Atomizer {available_atomizers} ")
    return available_atomizers[atomizer_name.upper()]()
