import argostranslate.package
import argostranslate.translate


class Translator:
    def __init__(self) -> None:
        # Download and install Argos Translate package
        argostranslate.package.update_package_index()
        available_packages = argostranslate.package.get_available_packages()
        package_to_install = next(
            filter(
                lambda x: x.from_code == "it" and x.to_code == "en", available_packages
            )
        )
        argostranslate.package.install_from_path(package_to_install.download())
        package_to_install = next(
            filter(
                lambda x: x.from_code == "en" and x.to_code == "it", available_packages
            )
        )
        argostranslate.package.install_from_path(package_to_install.download())

    def it_to_eng(self, text: str) -> str:
        return argostranslate.translate.translate(text, "it", "en")

    def eng_to_it(self, text: str) -> str:
        return argostranslate.translate.translate(text, "en", "it")
