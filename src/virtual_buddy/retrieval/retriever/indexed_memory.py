from virtual_buddy.retrieval.embedder.core import Embedder

from virtual_buddy.document.knowledge_base import KnowledgeBase
from virtual_buddy.document.chapter import ChapterWithDataAtom

from virtual_buddy.retrieval.vectorstore.weaviate_vectorstore import (
    WeaviateVectorstore,
)
import pickle
from pathlib import Path
import tqdm
from loguru import logger
from typing import List, Dict, Any
from pydantic import BaseModel
from virtual_buddy.retrieval.retriever.emedding_with_metadata import (
    EmbeddingWithMetadata,
)


class IndexedMemory(BaseModel):
    all_embedding_with_metadata: List[EmbeddingWithMetadata]
    embedder_config: Dict[str, Any]
    vectorstore_class: str


def create_indexed_memory(
    kb: KnowledgeBase, vectorstore: WeaviateVectorstore, embedder: Embedder
) -> IndexedMemory:
    all_emedded_atoms = get_all_embedding_with_metadata(kb, embedder)
    indexed_memory = IndexedMemory(
        all_embedding_with_metadata=all_emedded_atoms,
        embedder_config=embedder.get_config(),
        vectorstore_class=str(vectorstore.__class__),
    )
    return indexed_memory


def store_indexed_memory(
    indexed_memory: IndexedMemory,
    path_to_index_folder: str,
) -> None:
    # Path(path_to_index_folder).mkdir(parents=True, exist_ok=True)

    with open(path_to_index_folder, "wb") as outfile:
        pickle.dump(indexed_memory, outfile)


def load_indexed_memory(
    path_to_indexed_memory: str,
) -> IndexedMemory:
    # load json from path_to_indexed_memory
    with open(path_to_indexed_memory, "rb") as infile:
        indexed_memory = pickle.load(infile)
    return indexed_memory


def get_all_embedding_with_metadata(
    kb: KnowledgeBase, embedder: Embedder
) -> List[EmbeddingWithMetadata]:
    all_emedded_atoms = []

    for doc in kb.documents:
        logger.info("Embedding all document in knowledge base")
        for chapter in tqdm.tqdm(doc.chapters):
            if isinstance(chapter, ChapterWithDataAtom):
                for data_atom in chapter.data_atoms:
                    embedding = embedder.embed(data_atom.text)
                    all_emedded_atoms.append(
                        EmbeddingWithMetadata(
                            data_atom_id=data_atom.id,
                            chapter_id=data_atom.parent_chapter_id,
                            data_atom_text=data_atom.text,
                            embedding_as_list=embedding.tolist()[0],
                            document_name=doc.name,
                        )
                    )

    return all_emedded_atoms
