from typing import Dict, List
from pydantic import BaseModel
import numpy as np


class EmbeddingWithSecondLevel(BaseModel):
    embedding_as_list: List[float]
    document_name: str

    @property
    def embedding(self) -> np.ndarray:
        return np.array(self.embedding_as_list)

    class Config:
        arbitrary_types_allowed = True

    def get_properties(self) -> Dict[str, str]:
        copy_as_dict = self.__dict__.copy()
        copy_as_dict.pop("embedding_as_list")
        return copy_as_dict

    def get_schema_properties(self) -> List[str]:
        return list((self.get_properties()).keys())


class AllPageEmbedding(BaseModel):
    al_page_text: str
    embedding_as_list: List[float]
    document_name: str

    @property
    def embedding(self) -> np.ndarray:
        return np.array(self.embedding_as_list)

    class Config:
        arbitrary_types_allowed = True

    def get_properties(self) -> Dict[str, str]:
        copy_as_dict = self.__dict__.copy()
        copy_as_dict.pop("embedding_as_list")
        return copy_as_dict

    def get_schema_properties(self) -> List[str]:
        return list((self.get_properties()).keys())


class ChapterEmbedding(BaseModel):
    chapter_text: str
    embedding_as_list: List[float]
    document_name: str

    @property
    def embedding(self) -> np.ndarray:
        return np.array(self.embedding_as_list)

    class Config:
        arbitrary_types_allowed = True

    def get_properties(self) -> Dict[str, str]:
        copy_as_dict = self.__dict__.copy()
        copy_as_dict.pop("embedding_as_list")
        return copy_as_dict

    def get_schema_properties(self) -> List[str]:
        return list((self.get_properties()).keys())


class EmbeddingWithMetadata(BaseModel):
    data_atom_id: int
    chapter_id: str
    data_atom_text: str
    embedding_as_list: List[float]
    document_name: str

    @property
    def embedding(self) -> np.ndarray:
        return np.array(self.embedding_as_list)

    class Config:
        arbitrary_types_allowed = True

    def get_properties(self) -> Dict[str, str]:
        copy_as_dict = self.__dict__.copy()
        copy_as_dict.pop("embedding_as_list")
        return copy_as_dict

    def get_schema_properties(self) -> List[str]:
        return list((self.get_properties()).keys())
