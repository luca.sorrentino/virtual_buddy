from typing import List, Dict, Union, Any
from virtual_buddy.document.chapter import Chapter
from virtual_buddy.document.knowledge_base import KnowledgeBase


class RetrievedData:
    def __init__(
        self,
        text: str,
        chapter_id: str,
        document_name: str,
        score: float,
        retrieved_chapter: Chapter,
    ):
        self.text = text
        self.chapter_id = chapter_id
        self.document_name = document_name
        self.score = score
        self.retrieved_chapter = retrieved_chapter


def extract_medatadata_from_retrieved_data(
    retrieved_data: List[Dict[str, Any]], kb: KnowledgeBase
) -> List[RetrievedData]:
    all_retrieved_data = []

    for embedding_with_metadata in retrieved_data:
        chapter_id = embedding_with_metadata["chapter_id"]
        data_atom_text = embedding_with_metadata["data_atom_text"]
        document_name = embedding_with_metadata["document_name"]
        score = embedding_with_metadata["_additional"]["score"]

        retrieved_chapter = kb.all_chapters_map[chapter_id]

        all_retrieved_data.append(
            RetrievedData(
                text=data_atom_text,
                chapter_id=chapter_id,
                document_name=document_name,
                score=score,
                retrieved_chapter=retrieved_chapter,
            )
        )
    return all_retrieved_data
