from abc import ABC, abstractmethod
from typing import List, Dict
from virtual_buddy.retrieval.vectorstore.weaviate_vectorstore import (
    WeaviateVectorstore,
)
import numpy as np
from virtual_buddy.retrieval.embedder.core import Embedder
from virtual_buddy.document.knowledge_base import KnowledgeBase
from virtual_buddy.retrieval.retriever.indexed_memory import IndexedMemory
from virtual_buddy.document.document import Document
from functools import partial
from sklearn.metrics.pairwise import cosine_similarity
from sentence_transformers.util import cos_sim
import numpy as np
from sentence_transformers import SentenceTransformer, CrossEncoder, util

# get the closer one
from sklearn.metrics.pairwise import (
    pairwise_distances_argmin,
    pairwise_distances,
)


class EmbeddingRetrieveStrategy(ABC):
    def __init__(
        self,
        vectorstore: WeaviateVectorstore,
    ):
        self.vectorstore = vectorstore

    @abstractmethod
    def __call__(self, query: np.ndarray, top_n: int) -> List[Dict[str, str]]:
        pass


class HibrydRetrieveStrategy:
    def __init__(self, vectorstore: WeaviateVectorstore, alpha: float):
        self.alpha = alpha
        self.vectorstore = vectorstore

    def __call__(
        self,
        query_text: str,
        embedded_query: list[float],
    ) -> List[Dict[str, str]]:
        query_schema = [
            "data_atom_id",
            "chapter_id",
            "data_atom_text",
            "document_name",
        ]

        extracted_docs = self.vectorstore.retrieve_with_hyrbrid_search(
            query_text,
            embedded_query,
            query_schema,
            self.alpha,
            class_name="EmbeddingWithMetadata",
        )

        return extracted_docs


class RerankingCrossEncoder(EmbeddingRetrieveStrategy):
    def __init__(
        self,
        kb,
    ):
        # The bi-encoder will retrieve 100 documents. We use a cross-encoder, to re-rank the results list to improve the quality
        self.cross_encoder = CrossEncoder("cross-encoder/ms-marco-MiniLM-L-6-v2")
        self.kb = kb

    # creare un metodo che ricalcolo la lista di priorità da dare ai risultati in base
    # alla loro distanza dall' embedding con tutti i titoli della pagina
    def rescoring(self, result_value: Dict[str, str], text_query: str):
        chapter_with_data_atom = self.kb.all_chapters_map[result_value["chapter_id"]]
        # retrieved_chapter_text = (
        #    f"({chapter_with_data_atom.document_name}) {chapter_with_data_atom.title}:"
        #    f" \n {chapter_with_data_atom.text}"
        # )
        retrieved_chapter_text = (
            f"{chapter_with_data_atom.document_name}  {chapter_with_data_atom.text}"
        )

        ##### Re-Ranking #####
        # Now, score all retrieved passages with the cross_encoder
        cross_inp = [text_query, retrieved_chapter_text]
        cross_scores = self.cross_encoder.predict(cross_inp)

        return cross_scores

    def __call__(
        self, text_query: str, first_level_search: List[Dict[str, str]]
    ) -> List[Dict[str, str]]:
        # trucco per assegnare in anticipo a rescoring uno dei due parametri
        rescoring_method = partial(self.rescoring, text_query=text_query)

        # Ordina la lista di parole in base al peso
        reordered_first_level_search = sorted(
            first_level_search, key=rescoring_method, reverse=True
        )

        return reordered_first_level_search


class RerankingWithDocName(EmbeddingRetrieveStrategy):
    def __init__(
        self,
        embedder,
    ):
        self.embedder = embedder

    # creare un metodo che ricalcolo la lista di priorità da dare ai risultati in base
    # alla loro distanza dall' embedding con tutti i titoli della pagina
    def rescoring(self, result_value: Dict[str, str], embedded_query: List[float]):
        np_embedded_query = np.expand_dims(np.array(embedded_query), axis=0)
        embedding_doc_name = self.embedder.embed(result_value["document_name"])[0]

        np_embedding_doc_name = np.expand_dims(np.array(embedding_doc_name), axis=0)
        new_distance = cos_sim(np_embedding_doc_name, np_embedded_query)

        return new_distance

    def __call__(
        self, embedded_query: List[float], first_level_search: List[Dict[str, str]]
    ) -> List[Dict[str, str]]:
        # trucco per assegnare in anticipo a rescoring uno dei due parametri
        rescoring_method = partial(self.rescoring, embedded_query=embedded_query)

        # Ordina la lista di parole in base al peso
        reordered_first_level_search = sorted(first_level_search, key=rescoring_method)

        return reordered_first_level_search


class RerankingWithTitles(EmbeddingRetrieveStrategy):
    def __init__(
        self,
        mapper: Dict[str, list[float]],
    ):
        self.mapper = mapper

    # creare un metodo che ricalcolo la lista di priorità da dare ai risultati in base
    # alla loro distanza dall' embedding con tutti i titoli della pagina
    def rescoring(self, result_value: Dict[str, str], embedded_query: List[float]):
        np_embedded_query = np.expand_dims(np.array(embedded_query), axis=0)
        retrieved_doc_name = result_value["document_name"]
        total_page_emb_retrieved = self.mapper[retrieved_doc_name]
        np_total_page_emb_retrieved = np.expand_dims(
            np.array(total_page_emb_retrieved), axis=0
        )
        new_distance = 1 - cosine_similarity(
            np_total_page_emb_retrieved, np_embedded_query
        )
        return new_distance

    def __call__(
        self, embedded_query: List[float], first_level_search: List[Dict[str, str]]
    ) -> List[Dict[str, str]]:
        # trucco per assegnare in anticipo a rescoring uno dei due parametri
        rescoring_method = partial(self.rescoring, embedded_query=embedded_query)

        # Ordina la lista di parole in base al peso
        reordered_first_level_search = sorted(first_level_search, key=rescoring_method)

        return reordered_first_level_search


class NaiveRetrieveStrategy(EmbeddingRetrieveStrategy):
    def __call__(self, embedded_query: np.ndarray, top_n: int) -> List[Dict[str, str]]:
        query_schema = ["data_atom_id", "chapter_id", "data_atom_text", "document_name"]
        extracted_docs = self.vectorstore.retrieve_with_vector(
            embedded_query, top_n, query_schema, class_name="EmbeddingWithMetadata"
        )

        return extracted_docs


class NaiveChapterRetrieveStrategy(EmbeddingRetrieveStrategy):
    def __call__(self, embedded_query: np.ndarray, top_n: int) -> List[Dict[str, str]]:
        query_schema = ["chapter_text", "document_name"]
        extracted_docs = self.vectorstore.retrieve_with_vector(
            embedded_query, top_n, query_schema, class_name="ChapterEmbedding"
        )

        return extracted_docs


class NaiveRetrieveTriplo(EmbeddingRetrieveStrategy):
    def __call__(self, embedded_query: np.ndarray, top_n: int) -> List[Dict[str, str]]:
        query_schema = ["data_atom_id", "chapter_id", "data_atom_text", "document_name"]
        first_level = self.vectorstore.retrieve_with_vector(
            embedded_query, top_n, query_schema, class_name="EmbeddingWithMetadata"
        )

        query_schema = ["document_name"]
        second_level = self.vectorstore.retrieve_with_vector(
            embedded_query, top_n, query_schema, class_name="EmbeddingWithSecondLevel"
        )

        assert 1 == 2
        query_schema = ["al_page_text", "document_name"]
        third_level = self.vectorstore.retrieve_with_vector(
            embedded_query, top_n, query_schema, class_name="AllPageEmbedding"
        )

        assert 1 == 2

        return extracted_docs
