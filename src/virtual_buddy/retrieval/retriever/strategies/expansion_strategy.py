from abc import ABC, abstractmethod
from typing import List, Tuple, Dict, Any
from virtual_buddy.retrieval.vectorstore.weaviate_vectorstore import (
    WeaviateVectorstore,
)
from virtual_buddy.document.data_atom import DataAtom
import numpy as np
from virtual_buddy.document.knowledge_base import KnowledgeBase
from virtual_buddy.agents.tokenizer import Tokenizer
import re
from virtual_buddy.retrieval.embedder.core import Embedder
from virtual_buddy.retrieval.retriever.retriever import (
    RetrievedData,
)


def all_documents_text(
    retrieved_metadata: List[RetrievedData], kb: KnowledgeBase, max_num_docs: int
) -> str:
    retrieve_docs = set()
    context = ""
    for retrieved_data in retrieved_metadata:
        if retrieved_data.document_name not in retrieve_docs:
            if max_num_docs == 0:
                break
            retrieve_docs.add(retrieved_data.document_name)
            retrieved_document = kb.all_document_map[retrieved_data.document_name]
            context += (
                " <Start New Document> \n"
                f"\n\t <Document name>: {retrieved_document.name} \n"
                f"\n\t <Document text>: {retrieved_document.text}"
                "\n <End New Document>\n"
            )
            max_num_docs -= 1

    return context


def top1_chapter_text(retrieved_metadata: List[RetrievedData]) -> str:
    return retrieved_metadata[0].retrieved_chapter.text


def top3_chapter_text(retrieved_metadata: List[RetrievedData]) -> str:
    context = (
        f" First resource: \n {retrieved_metadata[0].retrieved_chapter.title}: \n"
        f" {retrieved_metadata[0].retrieved_chapter.text} \n\n "
        f" Second resource: \n {retrieved_metadata[1].retrieved_chapter.title}: \n"
        f" {retrieved_metadata[1].retrieved_chapter.text} \n\n "
        f" Third resource: \n {retrieved_metadata[2].retrieved_chapter.title}: \n"
        f" {retrieved_metadata[2].retrieved_chapter.text} \n\n "
    )

    return context


"""
class AtomExpansionStrategy(ABC):
    def __init__(self, kb: KnowledgeBase, vector_store: WeaviateVectorstore):
        self.kb = kb
        self.memory_bridge = vector_store

    @abstractmethod
    def __call__(
        self,
        previous_atoms: List[str],
        retireved_atom: str,
        next_atoms: List[str],
        direction: str,
    ) -> str:
        pass


#################################


class NotExpand(AtomExpansionStrategy):
    def __init__(
        self,
        kb: KnowledgeBase,
        vectorstore: WeaviateVectorstore,
        tokenizer: Tokenizer,
        token_max: int,
    ):
        super().__init__(kb, vectorstore)
        self.tokenizer = tokenizer
        self.token_max = token_max

    def __call__(
        self,
        all_previous_atoms_text: List[str],
        retireved_atom_text: str,
        all_next_atom_text: List[str],
        direction: str,
    ) -> str:
        return retireved_atom_text


class ExpandUntilTokensLeft(AtomExpansionStrategy):
    def __init__(
        self,
        kb: KnowledgeBase,
        vectorstore: WeaviateVectorstore,
        tokenizer: Tokenizer,
        token_max: int,
    ):
        super().__init__(kb, vectorstore)
        self.tokenizer = tokenizer
        self.token_max = token_max

    def __call__(
        self,
        all_previous_atoms_text: List[str],
        retireved_atom_text: str,
        all_next_atom_text: List[str],
        direction: str,
    ) -> str:
        look_ahead = 0
        shuld_go_forward = direction == "forward" or direction == "both"
        shuld_go_back = direction == "backward" or direction == "both"
        all_retrieved_text = ""
        token_left = self.token_max

        token_left -= self.tokenizer.token_count(retireved_atom_text)
        if token_left >= 0:
            all_retrieved_text = retireved_atom_text

            while token_left > 0:
                if shuld_go_back and look_ahead < len(all_previous_atoms_text):
                    token_left -= self.tokenizer.token_count(
                        all_previous_atoms_text[look_ahead]
                    )
                    if token_left >= 0:
                        all_retrieved_text = (
                            all_previous_atoms_text[look_ahead]
                            + " "
                            + all_retrieved_text
                        )
                elif shuld_go_forward and look_ahead < len(all_next_atom_text):
                    token_left -= self.tokenizer.token_count(
                        all_next_atom_text[look_ahead]
                    )
                    if token_left >= 0:
                        all_retrieved_text = (
                            all_retrieved_text + " " + all_next_atom_text[look_ahead]
                        )
                else:
                    break

                look_ahead += 1

        return all_retrieved_text


def clean_atom_text(text: str) -> str:
    return re.sub(r"\s+", " ", text)


class AtomRetrieveStrategy:
    def __init__(self, kb: KnowledgeBase):
        self.kb = kb

    def __call__(
        self, extracted_docs: Dict[str, Any], direction: str
    ) -> Tuple[List[str], str, List[str]]:
        data_atom_id = extracted_docs["data_atom_id"]
        chapter_id = extracted_docs["chapter_id"]

        retireved_atom = self.kb.all_data_atoms_map.get(
            (chapter_id, data_atom_id), None
        )
        if retireved_atom is not None:
            retrieved_atom_text = clean_atom_text(retireved_atom.text)

            previous_atoms: List[str] = []
            next_atoms: List[str] = []

            if direction == "backward" or direction == "both":
                current_atom = retireved_atom
                while not current_atom.is_first_atom:  # type: ignore
                    current_atom = current_atom.prev  # type: ignore
                    current_atom_text = clean_atom_text(current_atom.text)  # type: ignore
                    previous_atoms.insert(0, current_atom_text)  # type: ignore
            if direction == "forward" or direction == "both":
                current_atom = retireved_atom
                while not current_atom.is_last_atom:  # type: ignore
                    current_atom = current_atom.next  # type: ignore
                    current_atom_text = clean_atom_text(current_atom.text)  # type: ignore
                    next_atoms.append(current_atom_text)  # type: ignore

            return previous_atoms, retrieved_atom_text, next_atoms
        else:
            return [], "", []
"""
