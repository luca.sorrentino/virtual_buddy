from virtual_buddy.retrieval.embedder.core import Embedder
import random
import numpy as np


class FakeEmbedder(Embedder):
    def embed(self, text: str) -> np.ndarray:
        return np.array([random.random() for _ in range(10)])

    def get_config(self) -> dict[str, str]:
        return {"name": "fake_embedder"}
