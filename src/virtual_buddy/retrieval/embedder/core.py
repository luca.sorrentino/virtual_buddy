from abc import ABC, abstractmethod
import numpy as np
from typing import Dict, Any


class Embedder(ABC):
    def __init__(self, *args, **kwargs) -> None:
        super().__init__()

    @abstractmethod
    def embed(self, text: str) -> np.ndarray:
        pass

    @abstractmethod
    def get_config(self) -> Dict[str, Any]:
        pass
