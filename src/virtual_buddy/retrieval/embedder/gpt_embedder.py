from virtual_buddy.retrieval.embedder.core import Embedder
import numpy as np
from typing import Dict, Any
from openai import OpenAI

client = OpenAI(api_key="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")
import json


# text-embedding-ada-002
class OpenAiEmbedder(Embedder):
    def __init__(self, model_name: str, *args, **kwargs):
        self.model_name = model_name

    def embed(self, text: str) -> np.ndarray:
        
        response = client.embeddings.create(model=self.model_name, input=text)
        embedding = np.array([response.data[0]["embedding"]])  # type: ignore
        return embedding

    def get_config(self) -> Dict[str, Any]:
        return {
            "embedder_class": "OpenAiEmbedder",
            "model_name": self.model_name,
        }
