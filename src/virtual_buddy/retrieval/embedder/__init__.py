from virtual_buddy.retrieval.embedder.sbert_embedder import SBertEmbedder
from virtual_buddy.retrieval.embedder.gpt_embedder import OpenAiEmbedder
from virtual_buddy.retrieval.embedder.core import Embedder
from typing import Any, Dict


def get_embedder(embedder_conf: Dict[str, Any]) -> Embedder:
    available_embedders = {
        "SBERTEMBEDDER": SBertEmbedder,
        "OPENAIEMBEDDER": OpenAiEmbedder,
    }

    # print(f"Embedder {available_embedders} ")
    return available_embedders[embedder_conf["embedder_class"].upper()](**embedder_conf)
