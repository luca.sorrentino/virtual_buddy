from virtual_buddy.retrieval.embedder.core import Embedder
from sentence_transformers import SentenceTransformer  # type: ignore
import numpy as np
from typing import Dict, Any
import os


class SBertEmbedder(Embedder):
    def __init__(self, model_name: str, *args, **kwargs):
        # Explicitly set the environment variable TOKENIZERS_PARALLELISM=(true | false)
        os.environ["TOKENIZERS_PARALLELISM"] = "false"

        self.model = SentenceTransformer(model_name)
        self.model_name = model_name

    def embed(self, text: str) -> np.ndarray:
        input_sentence = [text]
        # Sentences are encoded by calling model.encode()
        embeddings = self.model.encode(input_sentence)
        return embeddings

    def get_config(self) -> Dict[str, Any]:
        return {
            "embedder_class": "SBertEmbedder",
            "model_name": self.model_name,
        }
