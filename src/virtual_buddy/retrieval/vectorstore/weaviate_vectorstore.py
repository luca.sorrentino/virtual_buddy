from loguru import logger
from tqdm.auto import tqdm
from typing import List, Dict, Literal, Any
from pydantic import BaseModel
import numpy as np
from virtual_buddy.retrieval.retriever.emedding_with_metadata import (
    EmbeddingWithMetadata,
)
from weaviate import Client  # type: ignore


class WeaviateVectorstore:
    def __init__(self, load_batch_size: int):
        vector_store_url = "http://localhost:8080"
        self.client = Client(url=vector_store_url)
        logger.info(f"Connect to vector store at {vector_store_url}")
        self.load_batch_size = load_batch_size

    def reset_vectorstore(self, class_name: str) -> None:
        # We define the object class with both BM25 and Vector HNSW index
        self.remove_class(class_name)
        self.add_new_class(class_name)

    def add_new_class(self, class_name: str) -> None:
        # We define the object class with both BM25 and Vector HNSW index
        self.client.schema.create_class({"class": class_name})

    def remove_class(self, class_name: str) -> None:
        # We define the object class with both BM25 and Vector HNSW index
        self.client.schema.delete_class(class_name)

    def populate_vectorstore(self, records: List[Any], class_name: str) -> None:
        logger.info(f"Loading {len(records)} documents into the database!")
        # record_type = self.class_name
        with self.client.batch as batch:
            batch.batch_size = self.load_batch_size
            for record in records:
                self.client.batch.add_data_object(
                    record.get_properties(),
                    class_name,
                    vector=record.embedding,
                )

    def retrieve_with_hyrbrid_search(
        self,
        query: str,
        query_embedding: List[float],
        queried_schema: List[str],
        alpha: float,
        class_name: str,
    ):
        logger.info("Run hybrid search")
        result = (
            self.client.query.get(class_name, queried_schema)
            .with_hybrid(query=query, vector=query_embedding, alpha=alpha)
            .with_additional(["score"])
            .do()
        )
        extracted_docs = result["data"]["Get"][class_name]
        logger.debug(f"Hybrid search done. Retrieved {len(extracted_docs)}")
        return extracted_docs

    def retrieve_with_keywords(
        self, query: str, queried_schema: List[str], class_name: str
    ):
        logger.info("Run bm25 search")

        result = (
            self.client.query.get(class_name, queried_schema)
            .with_bm25(query)
            .with_additional(["score"])
            .do()
        )
        extracted_docs = result["data"]["Get"][class_name]
        logger.debug(f"BM25 search done. Retrieved {len(extracted_docs)}")
        return extracted_docs

    def retrieve_with_vector(
        self,
        query_embedding: np.ndarray,
        top_k: int,
        queried_schema: List[str],
        class_name: str,
    ) -> List[Dict[str, str]]:
        logger.info("Run vector search")
        near_vector: Dict[str, np.ndarray] = {"vector": query_embedding}

        result = (
            self.client.query.get(class_name, queried_schema)
            .with_near_vector(near_vector)
            .with_limit(top_k)
            .with_additional(["certainty", "id", "distance"])
            # .with_where({
            #    "path": ["certainty"],
            #    "operator": "GreaterThan",
            #    "valueNumber": 0.01
            # })
            .do()
        )
        try:
            extracted_docs = result["data"]["Get"][class_name]
            logger.debug(f"Vector search done. Retrieved {len(extracted_docs)} results")
            return extracted_docs
        except KeyError as e:
            raise Exception(
                f"Data not found in the vectorstore: {result['errors'][0]['message']}"
            ) from e
