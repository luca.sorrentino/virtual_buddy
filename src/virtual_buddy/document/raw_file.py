from pathlib import Path


class RawFile:
    def __init__(self, data: list[str], extension: str, path: str) -> None:
        self.data = data
        self.extension = extension
        self.file_path = path
        self.filename = Path(path).stem

    def __repr__(self) -> str:
        return f"\n RawFile \n data={self.data} \n extension={self.extension}"
