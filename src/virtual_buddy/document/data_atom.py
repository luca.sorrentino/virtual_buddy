from typing import Union
from abc import ABC


class DataAtom(ABC):
    def __init__(
        self,
        text: Union[str, list[str]],
        prev: Union["DataAtom", None],
        next: Union["DataAtom", None],
        chunck_id: int,
        parent_chapter_id: str,
    ) -> None:
        if isinstance(text, list):
            text = " ".join(text)
        self.prev = prev
        self.next = next
        self.text = text
        self.parent_chapter_id = parent_chapter_id
        self.id = chunck_id

    @property
    def is_last_atom(self) -> bool:
        return self.next is None

    @property
    def is_first_atom(self) -> bool:
        return self.prev is None

    def __repr__(self) -> str:
        description_string = f"DataAtom ID: {self.id} \n text={self.text} \n"
        return description_string
