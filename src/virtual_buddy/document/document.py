from typing import List, Dict
from pathlib import Path
from virtual_buddy.document.chapter import Chapter


class Document:
    def __init__(self, name: str, path: Path | str, chapters: List[Chapter]) -> None:
        self.name = name
        self.path = Path(path)  # the path on disk
        self.chapters = chapters
        self.chapters_map: Dict[str, Chapter] = {
            chapter.id: chapter for chapter in chapters
        }

    @property
    def text(self):
        all_text = ""
        for chapter in self.chapters:
            all_text += (
                f"<Chapter title> {chapter.title} \n <Chapter text> {chapter.text} \n"
            )
        return all_text

    def __repr__(self) -> str:
        return f"\nDocument \n name={self.name} \n path={self.path}"
