import pickle
from pathlib import Path
from typing import List, Tuple, Dict
from virtual_buddy.document.document import Document
from virtual_buddy.document.chapter import Chapter, DataAtom
from loguru import logger
from virtual_buddy.document.chapter import ChapterWithDataAtom


class KnowledgeBase:
    def __init__(
        self,
        name: str,
        documents: List[Document],
    ) -> None:
        self.name = name
        self.documents = documents

        self.all_document_map = {doc.name: doc for doc in documents}
        # join toghether all chapters_dict present in all documents
        self.all_data_atoms_map: Dict[Tuple[str, int], DataAtom] = {}
        self.all_chapters_map: Dict[str, Chapter] = {}

        for document in documents:
            self.all_chapters_map.update(document.chapters_map)
            for chapter in document.chapters:
                if isinstance(chapter, ChapterWithDataAtom):
                    self.all_data_atoms_map.update(chapter.data_atoms_dict)


def store_kb(kb: KnowledgeBase, path: str, overwrite=False) -> None:
    import sys

    new_recursion_limit = 50000  # Set your desired recursion limit
    sys.setrecursionlimit(new_recursion_limit)

    Path(path).mkdir(parents=True, exist_ok=True)
    logger.info(f"Store knowledge base {kb.name} in {path}")

    if overwrite:
        if Path(f"{path}/{kb.name}.pkl").exists():
            Path(f"{path}/{kb.name}.pkl").unlink()

    # store kwnowledge base using pickle
    with open(f"{path}/{kb.name}.pkl", "wb") as outfile:
        pickle.dump(kb, outfile)


def load_kb(kb_path: str) -> KnowledgeBase:
    logger.info(f"Load knowledge base from {kb_path}")
    # load kwnowledge base using pickle
    with open(kb_path, "rb") as infile:
        kb = pickle.load(infile)
    return kb
