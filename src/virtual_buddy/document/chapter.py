from typing import List, Dict, Tuple
from virtual_buddy.document.data_atom import DataAtom
from abc import ABC
from hashlib import md5
import uuid


class Chapter(ABC):
    def __init__(self, title: str, text: str) -> None:
        self.title = title
        self.text = text
        self.document_name = None
        self.id: str = md5((text + title).encode("utf-8")).hexdigest()
        self.summarized_text = ""
        # self.id = str(uuid.uuid4())

    def reset_id(self):
        self.id: str = md5((self.text + self.title).encode("utf-8")).hexdigest()

    def __repr__(self) -> str:
        return (
            f"\n Chapter  \n ID:{self.id}   \n title=({self.title}) \n data={self.text}"
        )


class ChapterWithDataAtom(Chapter):
    def __init__(self, title: str, text: str, data_atoms: List[DataAtom]) -> None:
        super().__init__(title, text)
        self.data_atoms = data_atoms
        self.data_atoms_dict: Dict[Tuple[str, int], DataAtom] = {
            (self.id, data_atom.id): data_atom for data_atom in data_atoms
        }

    @classmethod
    def init_from_chapter(
        cls, chapter: Chapter, data_atoms: List[DataAtom]
    ) -> "ChapterWithDataAtom":
        return cls(chapter.title, chapter.text, data_atoms)
