from virtual_buddy.retrieval.embedder import get_embedder
from virtual_buddy.data_ingestion.atomizer import get_atomizer
from typing import List
from virtual_buddy.document.knowledge_base import load_kb
from virtual_buddy.document.document import Document
from virtual_buddy.pipelines.loading_pipeline.document_loading import (
    load_all_documents,
)
import time
from virtual_buddy.settings import ConfigManager
from virtual_buddy.agents.tokenizer import TikTokenizer
from virtual_buddy.retrieval.vectorstore.weaviate_vectorstore import (
    WeaviateVectorstore,
)
from virtual_buddy.retrieval.retriever.retriever import (
    extract_medatadata_from_retrieved_data,
    RetrievedData,
)
from virtual_buddy.retrieval.retriever.strategies.emb_retrieve_strategy import (
    HibrydRetrieveStrategy,
    NaiveRetrieveStrategy,
)
from virtual_buddy.retrieval.retriever.strategies.expansion_strategy import (
    all_documents_text,
)
from virtual_buddy.agents.llm_generator import LLM_Generator
from virtual_buddy.agents.llm_contextualizer import LLM_Contextualizer
from virtual_buddy.agents.llm_imagetext import LLM_Image2Text
from virtual_buddy.agents.llm_source_finder import LLM_Source_Finder

from typing import Tuple, Dict, Any
import numpy as np

from virtual_buddy.retrieval.translator import Translator

# set loguru to info
from loguru import logger

logger.remove()
logger.add("logs/info.log", level="INFO", rotation="1 MB")


class Chatbot:
    def __init__(self):
        self.conf_manager = ConfigManager()

        self.agent_classes = {
            "llm_generator_pro": LLM_Generator,
            "llm_contextualizer": LLM_Contextualizer,
            "llm_generator": LLM_Generator,
            "llm_image2text": LLM_Image2Text,
            "llm_source_finder": LLM_Source_Finder,
        }

        # Secret api key
        secret_api_key = self.conf_manager.get_secret_config("api_key")
        self.api_key = secret_api_key.api_key.get_secret_value()

        # get Tokenizer
        tokenizer_encoding_name = TikTokenizer.get_tokenizer(
            self.conf_manager.get_config("llm_generator")["model_name"]
        )

        self.top_n = self.conf_manager.get_config("retrieval")["top_n"]
        self.tokenizer = TikTokenizer(encoding_name=tokenizer_encoding_name.name)

        # get embedder
        self.embedder = get_embedder(self.conf_manager.get_config("embedder"))
        logger.info("Loaded Embedder")

        self.init_atomizer()
        logger.info("Initialized atomizer")

        self.generator_agent = self.init_agent("llm_generator")
        self.source_agent = self.init_agent("llm_source_finder")
        self.kb = self.get_kb()
        self.vector_store = WeaviateVectorstore(load_batch_size=500)

        self.max_allowed_tokens = self.conf_manager.get_config("llm_generator")[
            "max_allowed_tokens"
        ]
        self.expanding_direction = "forward"
        self.top_n = self.conf_manager.get_config("retrieval")["top_n"]
        alpha = self.conf_manager.get_config("retrieval")["alpha"]
        self.emb_retrieve_strategy = HibrydRetrieveStrategy(
            self.vector_store, alpha=alpha
        )
        self.translator = Translator()
        self.contextualizer_agent = self.init_agent("llm_contextualizer")
        self.agent_generator_pro = self.init_agent("llm_generator_pro")

    def init_agent(self, agent_name):
        agent_config = self.conf_manager.get_config(agent_name)
        model_name = agent_config["model_name"]
        personas = agent_config["personas"]
        temperature = agent_config["temperature"]

        agent_class = self.agent_classes[agent_name]

        agent = agent_class(model_name, self.api_key, personas, temperature)
        return agent

    def init_atomizer(self):
        logger.info("Initialized atomizer")
        # split parameters
        atomizer_step_conf = self.conf_manager.get_config("atomizer_step")
        self.atomizer = get_atomizer(atomizer_step_conf["atomizer"])

    def rephrased_question(self, question: str, history: List[str]) -> str:
        response_content, response_reason_finish, total_token_usage = (
            self.contextualizer_agent.contextualize_question(question, history)
        )

        if response_content is None:
            response_content = question

        return response_content

    def load_data(self):
        # downloader parameter
        downloader_step_conf = self.conf_manager.get_config("downloader_step")
        source_path = downloader_step_conf["source_path"]
        destination_path = downloader_step_conf["destination_path"]

        # parse parameters
        parser_step_conf = self.conf_manager.get_config("parser_step")
        granularity_level = parser_step_conf["granularity_level"]

        # atomizer parameters
        atomizer_step_conf = self.conf_manager.get_config("atomizer_step")
        atom_size = atomizer_step_conf["atom_size"]
        overlap_size = atomizer_step_conf["overlap_size"]

        all_documents: List[Document] = load_all_documents(
            source_path,
            destination_path,
            granularity_level,
            self.atomizer,
            atom_size,
            overlap_size,
        )  # type: ignore

        return all_documents

    def get_kb(self):
        logger.info("Loading KB")

        # save KB
        dump_step_conf = self.conf_manager.get_config("dump_step")
        kb_name = dump_step_conf["kb_name"]
        kb_store_path = dump_step_conf["kb_store_path"]

        kb = load_kb(f"{kb_store_path}/{kb_name}.pkl")
        return kb

    def embed_query(self, query: str) -> np.ndarray:
        embedded_query = self.embedder.embed(query)
        return embedded_query

    def extract_docs(self, query: str, top_n: int):
        logger.info("Retrieving data")
        embedded_query = self.embed_query(query)
        extracted_docs = self.emb_retrieve_strategy(query, embedded_query.tolist()[0])
        extracted_docs = extracted_docs[0:top_n]
        return extracted_docs

    def get_source(self, question: str):
        debug_information = self.retrieve_context(question)

        response_content, response_reason_finish, total_token_usage = (
            self.source_agent.send_message(question, debug_information["context"])
        )
        return response_content

    def retrieve_data(self, query: str, top_n: int) -> List[RetrievedData]:
        extracted_docs = self.extract_docs(query, top_n)
        metadata_result = extract_medatadata_from_retrieved_data(
            extracted_docs, self.kb
        )
        return metadata_result

    def extract_context(
        self, retrieved_metadata: List[RetrievedData], top_n: int
    ) -> str:
        logger.info("Extracting context from metadata")
        context = all_documents_text(retrieved_metadata, self.kb, top_n)
        return context

    def extract_debug_info(
        self, retrieved_metadata: List[RetrievedData]
    ) -> Dict[str, Any]:
        logger.info("Generate debug info from extracted metadata")

        all_document_titles = []
        all_document_score = []
        all_chapter_titles = []

        for data in retrieved_metadata:
            all_document_titles.append(data.document_name)
            all_document_score.append(data.score)
            all_chapter_titles.append(data.retrieved_chapter.title)

        return {
            "all_chapter_titles": all_chapter_titles,
            "all_document_titles": all_document_titles,
            "all_document_score": all_document_score,
        }

    def restrict_context(self, context: str):
        #  limit context to limit size requested by llm
        all_tokens = self.tokenizer.encode(context)

        prompt_tokens = len(
            self.tokenizer.encode(
                self.conf_manager.all_config["llm_generator"]["personas"]
            )
        )

        restricted_context = self.tokenizer.decode(
            all_tokens[0 : self.max_allowed_tokens - prompt_tokens - 100]
        )

        return restricted_context

    def rephrase_question(self, question: str, history: List[str]) -> str:
        # Rephrase question to make it self consistent
        if len(history) > 2:
            question = self.rephrased_question(question, history)
            logger.info(f"Rephrased question: {question}")
        return question

    def retrieve_context(self, question: str) -> Dict[str, Any]:
        logger.info("Receiving question. Start to process it")

        # Retrieve data
        start_time = time.time()
        question = self.translator.it_to_eng(question)
        logger.info(f"Question translated into: {question}")
        retrieved_metadata = self.retrieve_data(question, self.top_n)

        # Extracr debug info
        context = self.extract_context(retrieved_metadata, self.top_n)
        restricted_context = self.restrict_context(context)
        debug_information = self.extract_debug_info(retrieved_metadata)
        end_time = time.time()
        retrieval_time = end_time - start_time

        debug_information["context"] = restricted_context
        debug_information["retrieval_time"] = retrieval_time
        debug_information["rephrased_question"] = question
        return debug_information

    def answer_question(
        self, question: str, use_pro: bool
    ) -> Tuple[str, Dict[str, Any]]:
        debug_information = self.retrieve_context(question)

        start_time = time.time()
        if use_pro:
            response_content, response_reason_finish, total_token_usage = (
                self.generator_agent.send_message(
                    question, debug_information["context"]
                )
            )
        else:
            response_content, response_reason_finish, total_token_usage = (
                self.agent_generator_pro.send_message(
                    question, debug_information["context"]
                )
            )

        end_time = time.time()
        answer_generation_time = end_time - start_time

        debug_information["response_reason_finish"] = response_reason_finish
        debug_information["total_token_usage"] = total_token_usage
        debug_information["answer_generation_time"] = answer_generation_time
        return response_content, debug_information

    def answer_question_stream(
        self, question: str, use_pro: bool, history: List[str]
    ) -> tuple[Tuple[str, str, int], Dict[str, Any]]:
        rephrased_question = self.rephrase_question(question, history)
        debug_information = self.retrieve_context(rephrased_question)

        start_time = time.time()
        if use_pro:
            streaming_answer = self.agent_generator_pro.send_message_streaming(
                question, debug_information["context"]
            )
        else:
            streaming_answer = self.generator_agent.send_message_streaming(
                question, debug_information["context"]
            )
        end_time = time.time()
        answer_generation_time = end_time - start_time

        debug_information["answer_generation_time"] = answer_generation_time
        return streaming_answer, debug_information
