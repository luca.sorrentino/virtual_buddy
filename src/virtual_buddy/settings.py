from pydantic import BaseSettings, SecretStr
from omegaconf import OmegaConf
from typing import Any
from pathlib import Path
from datetime import datetime


class SecretConfig(BaseSettings):  # type: ignore
    api_key: SecretStr


class ConfigManager:
    def __init__(self) -> None:
        self.all_config = self._load_config("./config.yaml")
        self.all_secret_config = self._load_config("./.secret.yaml")

        # get current timestamp in format YYYY-MM-DD_HH-MM-SS and create a folder with that name
        self.run_folder_name = Path("./data/runs") / Path(
            datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
        )
        self.run_folder_name.mkdir(parents=True, exist_ok=True)

        # save all configs in the run folder
        self._dump_config(self.all_config, f"{self.run_folder_name}/config.yaml")

    def _dump_config(self, config: dict[str, Any], config_path: str) -> None:
        OmegaConf.save(OmegaConf.create(config), config_path)

    def _load_config(self, config_path: str) -> dict[str, Any]:
        conf = OmegaConf.load(config_path)
        return dict(OmegaConf.create(OmegaConf.to_yaml(conf, resolve=True)))

    def get_config(self, config_name: str) -> dict[str, Any]:
        if config_name in self.all_config:
            return self.all_config[config_name]
        else:
            raise KeyError(f"Config {config_name} not found in all configs")

    def get_secret_config(self, config_name: str) -> SecretConfig:
        if config_name in self.all_secret_config:
            return SecretConfig(**self.all_secret_config[config_name])
        else:
            raise KeyError(f"Config {config_name} not found in all configs")


if __name__ == "__main__":
    # load yaml file
    conf_manager = ConfigManager()
    conf = OmegaConf.load("./config.yaml")
    print(conf)
