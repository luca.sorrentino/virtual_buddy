from fastapi import FastAPI, Response  # ignore
from virtual_buddy.chatbot import Chatbot
from typing import Tuple, Union
from starlette.responses import StreamingResponse
from typing import List, Dict
from pydantic import BaseModel
import spacy

# Crea un'applicazione FastAPI
app = FastAPI()
chatbot = Chatbot()


# Definisci un endpoint per il microservizio
@app.get("/")
async def read_root():
    return {"message": "Hello world!"}


"""
@app.post("/extract_docs/")
async def extract_docs(question: str):
    extracted_docs = chatbot.extract_docs(question)
    return {"extracted_docs": extracted_docs}
"""


class QueryParams(BaseModel):
    question: str
    history: List[str]
    flag_pro: bool


def process_stream(streaming_answer: Tuple[str, str, int]):
    for chunck in streaming_answer:
        if not chunck.choices[0].finish_reason == "stop":  # type: ignore
            yield chunck.choices[0].delta.content  # type: ignore


@app.post("/answer_question/")
async def answer_question(
    params: QueryParams,
) -> Dict[str, Union[str, Dict[str, str]]]:
    question = params.question
    flag_pro = params.flag_pro
    history = params.history

    rephrased_question = chatbot.rephrase_question(question, history)

    answer, debug_info = chatbot.answer_question(rephrased_question, use_pro=flag_pro)
    return {"answer": answer, "debug_info": debug_info}


@app.post("/answer_question_stream/")
async def answer_question_stream(
    response: Response, params: QueryParams
) -> StreamingResponse:
    question = params.question
    flag_pro = params.flag_pro
    history = (
        []
    )  # eliminiamo la ricostruzione della domanda, mettere questa cosa in una chiamata a parte

    streaming_answer, _ = chatbot.answer_question_stream(
        question, use_pro=flag_pro, history=history
    )

    return StreamingResponse(
        process_stream(streaming_answer), media_type="text/event-stream"
    )


@app.post("/get_near_embeddings/")
async def get_near_embeddings(response: Response, params: QueryParams):
    question = params.question
    ntop = 6

    retrieved_data = chatbot.extract_docs(question, ntop)
    return {"retrieved_data": retrieved_data}


@app.post("/retrieve_context/")
async def retrieve_context(response: Response, params: QueryParams):
    question = params.question
    history = params.history

    rephrased_question = chatbot.rephrase_question(question, history)
    debug_info = chatbot.retrieve_context(rephrased_question)
    return {"debug_info": debug_info}


@app.post("/get_source/")
async def get_source(response: Response, params: QueryParams):
    question = params.question
    history = params.history

    rephrased_question = chatbot.rephrase_question(question, history)
    sources = chatbot.get_source(rephrased_question)
    print(sources)
    return {"sources": sources}


@app.post("/detect_language/")
async def detect_language(response: Response, params: QueryParams):
    nlp = spacy.load("en_core_web_sm")
    nlp.add_pipe("language_detector")
    doc = nlp(params.question)
    return {"language": doc._.language}


if __name__ == "__main__":
    import uvicorn

    uvicorn.run(app, host="0.0.0.0", port=8000)
