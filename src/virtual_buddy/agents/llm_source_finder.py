from typing import Tuple
from openai import OpenAI


# openai.Model.list()
## https://platform.openai.com/docs/guides/gpt-best-practices/six-strategies-for-getting-better-results


class LLM_Source_Finder:
    def __init__(
        self,
        model_name: str,
        api_key: str,
        personas: str,
        temperature: float,
    ):
        self.model_name = model_name
        self.personas = personas
        self.history = [{"role": "system", "content": personas}]
        self.temperature = temperature
        self.client = OpenAI(api_key=api_key)

    def fill_prompt(self, retrieved_context: str, user_question: str) -> str:
        filed_prompt = (
            f"context: <start_context> {retrieved_context} <end_context> \n"
            f"User question: {user_question}"
        )
        return filed_prompt

    def use_local_history(self, context: str):
        return [
            {"role": "system", "content": self.personas},
            {"role": "user", "content": context},
        ]

    def send_message(
        self, user_question: str, retrieved_context: str
    ) -> Tuple[str, str, int]:
        context = self.fill_prompt(retrieved_context, user_question)
        # self.history.append({"role": "user", "content": content})
        response = self.client.chat.completions.create(
            model=self.model_name, messages=self.use_local_history(context), temperature=self.temperature  # type: ignore
        )

        response_content = response.choices[0].message.content  # type: ignore
        response_reason_finish = response.choices[0].finish_reason  # type: ignore
        total_token_usage = response.usage.total_tokens  # type: ignore

        return response_content, response_reason_finish, total_token_usage  # type: ignore

    def send_message_streaming(
        self, user_question: str, retrieved_context: str
    ) -> Tuple[str, str, int]:
        context = self.fill_prompt(retrieved_context, user_question)
        # self.history.append({"role": "user", "content": content})
        streaming_response = self.client.chat.completions.create(
            model=self.model_name,
            messages=self.use_local_history(context),  # type: ignore
            temperature=self.temperature,
            stream=True,
        )  # type: ignore

        return streaming_response

    def add_to_history(self, role: str, content: str):
        self.history.append({"role": role, "content": content})
