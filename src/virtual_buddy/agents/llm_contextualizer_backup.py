from typing import Tuple, List, Dict, Literal
from openai import OpenAI

# openai.Model.list()
## https://platform.openai.com/docs/guides/gpt-best-practices/six-strategies-for-getting-better-results


class LLM_Contextualizer:
    def __init__(
        self,
        model_name: str,
        api_key: str,
        personas: str,
        temperature: float,
    ):
        self.model_name = model_name
        self.history = [{"role": "system", "content": personas}]
        self.temperature = temperature
        self.client = OpenAI(api_key=api_key)
        self.personas = personas

    def fill_prompt(self, text: str) -> str:
        filed_prompt = f"New Question: {text}"
        return filed_prompt

    def contextualize_question(self, new_question: str, history: List[str]) -> Tuple[
        str | None,
        Literal["stop", "length", "tool_calls", "content_filter", "function_call"],
        int,
    ]:
        """
        history = [
            "How to join a circle?",
            (
                "To join a circle at AgileLab, you should contact the role:Leadlink of"
                " the circle you are interested in and inform them of your desire to"
                " join. If you have never been part of a circle and are unsure which"
                " one might be the best fit for you, you can follow the guided hiring"
                " process."
            ),
        ]

        new_question = "and how to leave a circle at AgileLab?"

        """
        local_history = [
            {"role": "system", "content": self.personas},
            {"role": "user", "content": history[-2]},
            {"role": "system", "content": history[-1]},
            {"role": "user", "content": new_question},
        ]

        # context = self.fill_prompt(last_question, last_answer, new_question)
        # self.history.append({"role": "user", "content": content})
        response = self.client.chat.completions.create(
            model=self.model_name, messages=local_history, temperature=self.temperature  # type: ignore
        )

        response_content = response.choices[0].message.content  # type: ignore
        response_reason_finish = response.choices[0].finish_reason  # type: ignore
        total_token_usage = response.usage.total_tokens  # type: ignore

        return response_content, response_reason_finish, total_token_usage
