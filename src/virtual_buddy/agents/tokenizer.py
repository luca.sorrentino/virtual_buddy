# https://github.com/openai/openai-cookbook/blob/main/examples/How_to_count_tokens_with_tiktoken.ipynb
import tiktoken
from tiktoken import Encoding
from abc import ABC, abstractmethod


class Tokenizer(ABC):
    @abstractmethod
    def encode(self, text: str) -> list[int]:
        pass

    @abstractmethod
    def decode(self, tokens: list[int]) -> str:
        pass

    @abstractmethod
    def token_count(self, text: str) -> int:
        pass


class TikTokenizer(Tokenizer):
    def __init__(self, encoding_name: str):
        self.encoder = tiktoken.get_encoding(encoding_name)

    @classmethod
    def get_tokenizer(cls, model_name: str) -> Encoding:
        return tiktoken.encoding_for_model(model_name)

    def encode(self, text: str) -> list[int]:
        return self.encoder.encode(text)

    def decode(self, tokens: list[int]) -> str:
        return self.encoder.decode(tokens)

    def token_count(self, text: str) -> int:
        return len(self.encode(text))
