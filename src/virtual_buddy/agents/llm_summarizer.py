from typing import Tuple
from openai import OpenAI

client = OpenAI(api_key=api_key)

# openai.Model.list()
## https://platform.openai.com/docs/guides/gpt-best-practices/six-strategies-for-getting-better-results


class LLM_Summarizer:
    def __init__(
        self,
        model_name: str,
        api_key: str,
        personas: str,
        temperature: float,
    ):
        self.model_name = model_name
        self.history = [{"role": "system", "content": personas}]
        self.temperature = temperature
        

    def fill_prompt(self, text: str) -> str:
        filed_prompt = f"Chapter: {text}"
        return filed_prompt

    def send_message(self, text: str) -> Tuple[str, str, int]:
        content = self.fill_prompt(text)
        self.history.append({"role": "user", "content": content})
        response = client.chat.completions.create(model=self.model_name,
        messages=self.history,
        temperature=self.temperature)

        response_content = response.choices[0].message.content  # type: ignore
        response_reason_finish = response.choices[0].finish_reason  # type: ignore
        total_token_usage = response.usage.total_tokens  # type: ignore

        return response_content, response_reason_finish, total_token_usage
