from typing import Tuple
from openai import OpenAI
from pathlib import Path
import json

# openai.Model.list()
## https://platform.openai.com/docs/guides/gpt-best-practices/six-strategies-for-getting-better-results


class LLM_Image2Text:
    def __init__(
        self,
        model_name: str,
        api_key: str,
        personas: str,
        temperature: float,
    ):
        self.model_name = model_name
        self.temperature = temperature
        self.personas = personas
        self.client = OpenAI(api_key=api_key)

    def fake_extract_text(self, image_url: str) -> Tuple[str, str, int]:
        return "placeholder text from image", "None", 0

    def extract_text_with_cache(
        self, remote_url: str, image_path: str
    ) -> Tuple[str, str, int]:

        # Accesso alla cache
        cache_path = "./data/cache/"
        Path(cache_path).mkdir(parents=True, exist_ok=True)

        local_path = f"{cache_path}/{Path(image_path).stem}.json"

        if Path(local_path).exists():
            with open(str(local_path)) as data_file:
                response_content = json.load(data_file)
                return response_content, "from_cache", 0

        # Richiamo delle api remote
        response = self.client.chat.completions.create(
            model="gpt-4-vision-preview",
            messages=[
                {
                    "role": "user",
                    "content": [
                        {"type": "text", "text": self.personas},
                        {
                            "type": "image_url",
                            "image_url": {
                                "url": remote_url + image_path,
                            },
                        },
                    ],
                }
            ],
            max_tokens=2000,
        )

        response_content = response.choices[0].message.content  # type: ignore
        response_reason_finish = response.choices[0].finish_reason  # type: ignore
        total_token_usage = response.usage.total_tokens  # type: ignore

        # Scrittura del risultato in cache
        with open(local_path, "w", encoding="utf-8") as f:
            json.dump(response_content, f, ensure_ascii=False, indent=4)

        return response_content, response_reason_finish, total_token_usage

    def extract_text(self, image_url: str) -> Tuple[str, str, int]:
        response = self.client.chat.completions.create(
            model="gpt-4-vision-preview",
            messages=[
                {
                    "role": "user",
                    "content": [
                        {"type": "text", "text": self.personas},
                        {
                            "type": "image_url",
                            "image_url": {
                                "url": image_url,
                            },
                        },
                    ],
                }
            ],
            max_tokens=2000,
        )

        response_content = response.choices[0].message.content  # type: ignore
        response_reason_finish = response.choices[0].finish_reason  # type: ignore
        total_token_usage = response.usage.total_tokens  # type: ignore

        return response_content, response_reason_finish, total_token_usage
