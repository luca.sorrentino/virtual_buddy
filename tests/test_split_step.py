from virtual_buddy.data_ingestion.atomizer.word_count_atomizer import WordCountAtomizer
from virtual_buddy.pipelines.loading_pipeline.steps.split import (
    split_step,
    group_atoms,
)
import pytest
from virtual_buddy.document.chapter import Chapter


@pytest.mark.parametrize(
    "overlap_size",
    [-1, 1.2, 40, 1, 0],
    ids=[
        "negative overlap",
        "float overla",
        "high overla",
        "low overlap",
        "zero overlap",
    ],
)
@pytest.mark.parametrize(
    "chunk_size",
    [-1, 1.2, 40, 1, 0],
    ids=[
        "negative chunk_size",
        "float chunk_size",
        "high chunk_size",
        "low chunk_size",
        "zero chunk_size",
    ],
)
@pytest.mark.parametrize(
    "text",
    [
        "",
        "Hello world",
        "Hello world, this is a very very long test ",
        ["ciao", "come", "va"],
    ],
    ids=["Empty-text", "very short text", "long text", "list of strings"],
)
def test_invalid_input_split_step(text: str, chunk_size: int, overlap_size: int):
    atomizer = WordCountAtomizer()
    chapter = Chapter(text, text)

    if type(text) != str:
        with pytest.raises(TypeError):
            split_step(chapter, atomizer, chunk_size, overlap_size)
    elif len(text) == 0:
        with pytest.raises(ValueError):
            split_step(chapter, atomizer, chunk_size, overlap_size)

    elif chunk_size < 0 or overlap_size < 0:
        with pytest.raises(ValueError):
            split_step(chapter, atomizer, chunk_size, overlap_size)

    elif overlap_size >= chunk_size or chunk_size - overlap_size < 0:
        with pytest.raises(ValueError):
            split_step(chapter, atomizer, chunk_size, overlap_size)

    elif type(chunk_size) != int or type(overlap_size) != int:
        with pytest.raises(TypeError):
            split_step(chapter, atomizer, chunk_size, overlap_size)

    else:
        split_step(chapter, atomizer, chunk_size, overlap_size)


def test_word_count_atomizer():
    atomizer = WordCountAtomizer()
    assert len(atomizer.split_into_atoms("")) == 1
    assert len(atomizer.split_into_atoms("helloworld")) == 1
    assert len(atomizer.split_into_atoms("hello world")) == 2
    assert len(atomizer.split_into_atoms("hello world!")) == 2


@pytest.mark.parametrize(
    "chunk_size",
    [1, 4, 30],
)
@pytest.mark.parametrize(
    "overlap_size",
    [1, 2, 3],
)
def test_group_atoms(chunk_size: int, overlap_size: int):
    all_atoms = ["ciao", "come", "va", "oggi", "tutto", "bene", "?"]
    chunk_size = 30
    overlap_size = 2

    all_text_chunks = group_atoms(all_atoms, chunk_size, overlap_size)

    if chunk_size > len(all_atoms):
        assert len(all_text_chunks) == 1

    for text_chunks in all_text_chunks:
        assert len(text_chunks) == min(chunk_size, len(all_atoms))
