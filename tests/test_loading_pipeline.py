'''
from virtual_buddy.pipelines.ingestion_pipeline.steps.node import (
    CreateSimpleNode,
)

import pytest
from typing import List

# TODO creare delle fixtures per i test


@pytest.mark.parametrize(
    "text_chunks",
    [[], ["Hello", "World", "I", "am", "a", "test"], [""]],
    ids=["Empty List", "Non-empty List", "List with Empty String"],
)
def test_create_simple_node_step(text_chunks: List[str]):
    node_step = CreateSimpleNode()

    if not text_chunks:
        with pytest.raises(ValueError):
            node_step.create_nodes(text_chunks)
    else:
        nodes = node_step.create_nodes(text_chunks)
        assert len(nodes) == len(text_chunks)
        assert nodes[0].prev is None
        assert nodes[-1].next is None
        for index in range(1, len(nodes)):
            assert nodes[index].prev == nodes[index - 1]
            assert nodes[index - 1].next == nodes[index]
            assert nodes[index].text == text_chunks[index]


"""
Questo test si rompe perchè non tornano il giusto numero di nodi
[Node(node_id=25bf02a1-45ff-41cc-8ff9-167dac602485, text=Hel),
 Node(node_id=61fb01d1-9ced-4d35-9ef0-ece03b15e026, text=llor),
 Node(node_id=d1cd9cb7-f782-447a-b509-55d71080dfbc, text=reWo)]

# TODO aggiungere i parametri
def test_create_node_chapter():
    title = "Test Chapter"
    text = "HelloreWo"
    chunk_size = 3
    overlap_size = 1
    splitter = SplitterByWordCount()
    node_step = CreateSimpleNode()
    chapter_step = CreateNodesChapter(node_step, splitter, chunk_size, overlap_size)
    new_chapter = chapter_step.create_chapter(title, text)

    with pytest.raises(ValueError, match="The chapter title can not be empty"):
        chapter_step.create_chapter("", text)

    with pytest.raises(ValueError, match="The chapter text can not be empty"):
        chapter_step.create_chapter(title, "")

    assert new_chapter.title == title
    assert new_chapter.text == text
    assert len(new_chapter.nodes) == math.ceil(len(text) / chunk_size) + overlap_size
"""

"""
questo test non lo puoi fare se prima non fai le fixture
def test_create_simple_document():
    document_step = CreateSimpleDocument()
    name = "nome_documento"
    path = "./data/handbook/Sales/Presales/"
    parser = MdParser()
    node_step = CreateSimpleNode()
    splitter = SplitterByWordCount()
    chapter_granularity_level = 1
    node_window_size = 100
    node_overlap_size = 0

    chapter_step = CreateNodesChapter(
        node_step, splitter, node_window_size, node_overlap_size
    )

    new_document = document_step.create_document(
        name, path, parser, chapter_step, chapter_granularity_level
    )

    # controllare il numero di documenti creati

"""
'''
