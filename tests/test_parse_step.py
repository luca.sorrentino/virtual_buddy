from virtual_buddy.data_ingestion.parser.md_parser import MdParser
from mdutils.mdutils import MdUtils  # type: ignore
import pytest


def test_md_parser_out_boundary():
    splitter = MdParser()
    with pytest.raises(ValueError):
        list(splitter.get_chapters([""], 0))

    with pytest.raises(ValueError):
        list(splitter.get_chapters([""], 7))

    with pytest.raises(ValueError):
        list(splitter.get_chapters([""], -1))


@pytest.mark.parametrize(
    "paragraph_level",
    [1, 2, 3, 4, 5, 6],
    ids=lambda d: f"paragraph_level={d}",  # 0, 1, 3, 30
)
@pytest.mark.parametrize(
    "num_paragraphs", [0, 1, 3, 10], ids=lambda d: f"num_paragraphs={d}"  # 0, 1, 3, 30
)
def test_md_parser(num_paragraphs, paragraph_level):
    # TODO Aggiungere commenti per spiegare come questo
    # test testa il funzionamento di parser

    if num_paragraphs == 0:
        pytest.skip("Not of interest for now to empty md")

    mdFile = MdUtils(file_name="Example_Markdown")
    mdFile.create_md_file()

    # create previous paragraphs
    for header_level in range(1, paragraph_level):
        mdFile.new_header(
            level=header_level, title=f"{header_level} Atx Header {header_level}"
        )

    for num_p in range(num_paragraphs):
        mdFile.new_header(
            level=paragraph_level, title=f"{num_p} Atx Header {paragraph_level}"
        )

    markdown_text = mdFile.get_md_text().split()
    splitter = MdParser()

    found_paragraphs = list(splitter.get_chapters(markdown_text, paragraph_level))
    assert len(found_paragraphs) == num_paragraphs + (paragraph_level - 1)
