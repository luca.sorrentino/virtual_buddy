import streamlit as st
import pandas as pd
from frontend_utils import retrieve_context, stream_call_backend, get_current_history

# Creare un DataFrame per tenere traccia dei like e dislike
st.session_state["data"] = pd.read_csv("ui/data.csv")
st.session_state["retrieved_context"] = None
st.session_state["debug_info"] = None


# App title
st.set_page_config(page_title="💬 VirtualBuddy (nome provvisorio)")


if "messages" not in st.session_state.keys():
    st.session_state.messages = [
        {
            "role": "assistant",
            "content": (
                "Hi! I'm VirtualBuddy. If you have questions about AgileLab, feel"
                " free to ask me. I'll read the handbook and find the answer for you."
                " Otherwise, here's the link: handbook.agilelab.it"
            ),
        }
    ]

# Display chat messages
for message in st.session_state.messages:
    with st.chat_message(message["role"]):
        st.write(message["content"])


# User-provided prompt
if query := st.chat_input(disabled=False):
    st.session_state.messages.append({"role": "user", "content": query})
    with st.chat_message("user"):
        st.write(query)


# Generate a new response if the last message is not from the assistant
if st.session_state.messages[-1]["role"] != "assistant":
    st.session_state["rephrased_question"] = None
    full_response = ""

    with st.spinner("Reading question:"):
        debug_info = retrieve_context(query, st)
        if debug_info:
            if query and not debug_info["rephrased_question"].lower() == query.lower():
                st.session_state["rephrased_question"] = debug_info[
                    "rephrased_question"
                ]
                query = debug_info["rephrased_question"]
            st.session_state["debug_info"] = debug_info
            st.session_state["context"] = debug_info.get("context")
        if st.session_state["rephrased_question"]:
            full_response = (
                f"You are asking: {st.session_state['rephrased_question']} \n"
            )

    # st.session_state["query"] = debug_info["rephrased_question"]
    st.session_state["query"] = query
    show_button = True

    # Display response in a separate chat message
    with st.container():
        with st.chat_message("assistant"):
            with st.spinner("Thinking..."):
                placeholder = st.empty()
                full_response += "\n Answer: "
                placeholder.markdown(full_response)
                response = stream_call_backend(
                    "http://localhost:8000/answer_question_stream/",
                    debug_info["rephrased_question"],
                    get_current_history(st),
                    st.session_state["active_pro"],
                )
                placeholder_button = st.empty()
                # stop_button = st.button("⏹️")

                # if stop_button:
                #    full_response = ""
                for chunk in response:
                    full_response += chunk
                    placeholder.markdown(full_response)
                placeholder.markdown(full_response)

    if st.session_state["debug_info"]:
        with st.expander("Informazioni aggiuntive"):
            col1, col2 = st.columns(2)

            with col1:
                st.subheader("Retrieved titles")
                for id in range(
                    len(st.session_state["debug_info"]["all_document_titles"])
                ):
                    st.write(
                        "**Document name**:"
                        f" {st.session_state['debug_info']['all_document_titles'][id]} ->"
                        f" {st.session_state['debug_info']['all_chapter_titles'][id]} "
                    )

            with col2:
                st.subheader("debug_info")
                if st.session_state["debug_info"]:
                    st.write(
                        "retrieval_time:",
                        st.session_state["debug_info"]["retrieval_time"],
                    )
                    st.write(
                        "Rephrased question:",
                        st.session_state["debug_info"]["rephrased_question"],
                    )

    message = {"role": "assistant", "content": full_response}
    st.session_state.messages.append(message)


with st.sidebar:
    st.title("💬 VirtualBuddy (Temporary name)")
    st.session_state["active_pro"] = True
    # on = st.toggle("Activate Pro Version")
    # st.session_state["active_pro"] = True
    # if on:
    #    st.session_state["active_pro"] = True

    if st.button("👍"):
        new_row = dict()
        new_row["question"] = st.session_state["query"]
        new_row["evaluation"] = "positive"
        st.session_state["data"].loc[len(st.session_state["data"])] = new_row
        st.session_state["data"].to_csv("ui/data.csv", index=False)

    if st.button("👎"):
        new_row = dict()
        new_row["question"] = st.session_state["query"]
        new_row["evaluation"] = "negative"
        st.session_state["data"].loc[len(st.session_state["data"])] = new_row
        st.session_state["data"].to_csv("ui/data.csv", index=False)

    st.subheader("Retrieved Context")
    if st.session_state["debug_info"]:
        st.write(st.session_state["context"])

    st.markdown(
        "📖 Learn more about AgileLab with the"
        " [handbook](https://handbook.agilelab.it/)!"
    )
