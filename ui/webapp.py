import streamlit as st
import pandas as pd
from frontend_utils import (
    retrieve_context,
    stream_call_backend,
    call_backend,
    get_current_history,
)

# Creare un DataFrame per tenere traccia dei like e dislike
st.session_state["data"] = pd.read_csv("ui/data.csv")
st.session_state["retrieved_context"] = None
st.session_state["debug_info"] = None


# App title
st.set_page_config(page_title="💬 ChatBoost")


if "messages" not in st.session_state.keys():
    st.session_state.messages = [
        {
            "role": "assistant",
            "content": (
                # "Ciao, sono ChatBoost. Se avete domande su AgileLab, sentitevi"
                # " libero di chiedermele. Leggerò il manuale e troverò la risposta per"
                # " te. Altrimenti, ecco il link: https://handbook.agilelab.it/"
                "Hi! I'm VirtualBuddy. If you have questions about AgileLab, feel"
                " free to ask me. I'll read the handbook and find the answer for you."
                " Otherwise, here's the link: https://handbook.agilelab.it/"
            ),
        }
    ]

# Display chat messages
for message in st.session_state.messages:
    with st.chat_message(message["role"]):
        st.write(message["content"])


# User-provided prompt
if query := st.chat_input(disabled=False):
    st.session_state.messages.append({"role": "user", "content": query})
    with st.chat_message("user"):
        st.write(query)


# Generate a new response if the last message is not from the assistant
if st.session_state.messages[-1]["role"] != "assistant":
    full_response = ""
    debug_info = retrieve_context(query, st)

    # detected_language = call_backend(
    #    url="http://localhost:8000/detect_language/",
    #    question=query,
    #    history=[],
    #    flag_pro=False,
    # ).json()

    if debug_info:
        st.session_state["debug_info"] = debug_info
        st.session_state["context"] = debug_info.get("context")

    # st.session_state["query"] = debug_info["rephrased_question"]
    st.session_state["query"] = query

    # Display response in a separate chat message
    with st.container():
        with st.chat_message("assistant"):
            with st.spinner("I am thinking about it..."):
                placeholder = st.empty()
                full_response += "\n "
                placeholder.markdown(full_response)
                response = stream_call_backend(
                    "http://localhost:8000/answer_question_stream/",
                    query,
                    [],
                    st.session_state["active_pro"],
                )
                placeholder_button = st.empty()

                for chunk in response:
                    full_response += chunk
                    placeholder.markdown(full_response)
                placeholder.markdown(full_response)

    if st.session_state["debug_info"]:
        with st.expander("Additional Information"):
            col1, col2 = st.columns(2)

            with col1:
                response = call_backend(
                    "http://localhost:8000/get_source/",
                    query,
                    [],
                    st.session_state["active_pro"],
                )
                st.write("response: ", response.json())
                # st.subheader("Retrieved titles")
                # for id in range(
                #    len(st.session_state["debug_info"]["all_document_titles"])
                # ):
                #    st.write(
                #        "**Document name**:"
                #        f" {st.session_state['debug_info']['all_document_titles'][id]} ->"
                #        f" {st.session_state['debug_info']['all_chapter_titles'][id]} "
                #    )

            with col2:
                st.subheader("debug_info")
                if st.session_state["debug_info"]:
                    st.write(
                        "retrieval_time:",
                        st.session_state["debug_info"]["retrieval_time"],
                    )
                    st.write(
                        "Rephrased question:",
                        st.session_state["debug_info"]["rephrased_question"],
                    )

    message = {"role": "assistant", "content": full_response}
    st.session_state.messages.append(message)


with st.sidebar:
    st.title("💬 ChatBoost")
    st.session_state["active_pro"] = True
    # on = st.toggle("Activate Pro Version")
    # st.session_state["active_pro"] = True
    # if on:
    #    st.session_state["active_pro"] = True

    st.image("./ui/agilelab.png")
    st.markdown(
        "📖 Learn more about AgileLab with the"
        " [handbook](https://handbook.agilelab.it/)!"
    )

    # if st.session_state["debug_info"]:
    #    st.subheader("Retrieved Context")
    #    st.write(st.session_state["context"])
