import requests
import json
from typing import List, Union, Dict


def get_current_history(st):
    past_conversation = [
        conversation["content"] for conversation in st.session_state.messages
    ][:-1]
    return past_conversation


"""
# Function for generating LLM response
def generate_streaming_response(prompt_input, st):
    # URL del server FastAPI
    url = "http://localhost:8000/answer_question_stream/"
    past_conversation = get_current_history(st)
    flag_pro = st.session_state["active_pro"]

    # Dati da inviare nella richiesta POST
    data = {
        "question": prompt_input,
        "history": past_conversation,
        "flag_pro": flag_pro,
        "flag_stream": True,
    }

    # Invia la richiesta POST al server
    encoded_response = requests.post(url, json=data)

    if encoded_response.status_code == 200:
        for chunk in encoded_response.iter_content(chunk_size=1024):
            yield chunk.decode("utf-8")
"""


def stream_call_backend(url: str, question: str, history: List[str], flag_pro: bool):
    url = url

    data = {
        "question": question,
        "history": history,
        "flag_pro": flag_pro,
    }

    # Invia la richiesta POST al server
    encoded_response = requests.post(url, json=data, stream=True)

    if encoded_response.status_code == 200:
        for chunk in encoded_response.iter_content(chunk_size=1024):
            yield chunk.decode("utf-8")


def call_backend(url: str, question: str, history: List[str], flag_pro: bool):
    url = url

    data = {
        "question": question,
        "history": history,
        "flag_pro": flag_pro,
    }

    # Invia la richiesta POST al server
    encoded_response = requests.post(url, json=data)
    return encoded_response


def retrieve_context(prompt_input, st):
    url = "http://localhost:8000/retrieve_context/"
    history = get_current_history(st)
    flag_pro = st.session_state["active_pro"]
    encoded_response = call_backend(
        url=url,
        question=prompt_input,
        history=history,
        flag_pro=flag_pro,
    )

    if encoded_response.status_code == 500:
        debug_info = dict()
    else:
        # La richiesta è andata a buon fine
        response = encoded_response.json()
        debug_info = response.get(
            "debug_info"
        )  # Utilizza il metodo .get() per evitare KeyError

    return debug_info
